import React, { Component } from 'react';
import { SafeAreaView, ScrollView,StyleSheet, Text, Image, View, TouchableOpacity } from 'react-native';

export default class Screen2 extends Component{
    
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#e5e5e5'}}>
                <ScrollView horizontal style={styles.body}>
                    <View style={styles.screen}>
                        <Text style={styles.txt1}>Remittance Platforms</Text>
                        <Text style={styles.txt2}>Sistem pengiriman uang bagi pekerja imigran di Luar Negeri ke Indonesia</Text>
                        <Image style={styles.img} source={require('../assets/images/illustrasi1.png')}/>
                        <Text style={{ marginTop: 8, fontSize: 12 }}> {'<< '} geser ke kiri untuk pelajari lebih lanjut</Text>
                    </View>
                    <View style={styles.screen}>
                        <Text style={styles.txt1}>Kirim Uang Lebih Mudah</Text>
                        <Text style={styles.txt2}>Sekarang proses pengiriman uang lebih mudah, karena dapat Anda lakukan kapanpun dan dimana pun.</Text>
                        <Image style={styles.img} source={require('../assets/images/illustrasi2.png')}/>
                        <Text style={{ marginTop: 8, fontSize: 12 }}> {'<< '} geser ke kiri untuk pelajari lebih lanjut</Text>
                    </View>
                    <View style={styles.screen}>
                        <Text style={styles.txt1}>Pembayaran Tagihan</Text>
                        <Text style={styles.txt2}>Selain dapat melakukan pengiriman uang, Anda juga dapat melakukan pembayaran tagihan di Indonesia.</Text>
                        <Image style={styles.img} source={require('../assets/images/illustrasi3.png')}/>
                    </View>
                </ScrollView>
                <View style={{height:100, marginHorizontal:16}}>
                    <TouchableOpacity style={styles.box}
                    onPress={()=> this.props.navigation.navigate('Registrasi1')}>
                        <Text style={styles.txt3}>Buka Rekening Sekarang</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.box,{backgroundColor:'#153e74'}]}
                    onPress={()=> this.props.navigation.navigate('')}>
                        <Text style={[styles.txt3, {color:'white'}]}>Login</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    body:{
        flexDirection:'row',
        width:'93%',
        marginRight:5,
        flex:1,
        marginTop:15,
    },
    screen:{
        alignItems:'center',
        paddingHorizontal:16,
    },
    txt1:{
        fontSize:20,
        color:'#153e74',
        marginBottom:14,
    },
    txt2:{
        textAlign:'center',
        width:312,
        height:50,
    },
    img:{
        width:300,
        height: 350,
        resizeMode:'contain',
    },
    box:{
        width:'100%',
        borderRadius:3,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10,
        backgroundColor:'#e5e5e5',
        borderColor:'black',
        borderWidth:1,
    },
    txt3:{
        fontSize:14,
        color:'#153e74',
    }
})
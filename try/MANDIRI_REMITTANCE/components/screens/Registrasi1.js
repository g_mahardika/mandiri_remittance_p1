import React, { Component } from 'react';
import { View, StyleSheet,KeyboardAvoidingView, Text, Image, TouchableOpacity,TextInput, Modal, ScrollView, TouchableHighlight, } from 'react-native';
import CheckBox from 'react-native-check-box';
import {connect} from 'react-redux';
// import addFullName from '../../actions/fullName';

class Registrasi1 extends Component {

    constructor(props){
        super(props)
        this.state={
            pilihHubungan:'',
            isChecked: false,
            pickerSelection:'Pilih Hubungan :',
            pickerDisplayed:false,
            
            fullName:'',
        }
    }

    setPickerValue(newValue){
        this.setState({
            pickerSelection:newValue
        });
        this.togglePicker();
    }

    togglePicker(){
        this.setState({
            pickerDisplayed:!this.state.pickerDisplayed
        })
    }



    render(){
        // console.warn('>>', this.state.fullName)
        const pickerValues = [
            {title:'Ayah', value:'Ayah',},
            {title:'Ibu', value:'Ibu',},
            {title:'Saudara Laki-laki', value:'Saudara Laki-laki',},
            {title:'Saudara Perempuan', value:'Saudara Perempuan',}
        ]
        return(
            
            <View style={{flex:1}}>
                <TouchableOpacity style={styles.head}>
                    <Image source={require('../assets/images/Step.png')} style={styles.img} />
                </TouchableOpacity>
                    <ScrollView style={{backgroundColor:'#e5e5e5', paddingVertical:14, paddingHorizontal:16}}>
                        <Text style={[styles.txt, {marginTop:24}]}>Data & Dokumen Pribadi</Text>
                        <Text style={[{ color: '#828282', textAlign: 'center', marginTop: 8 }]}>
                            Pastikan data yang Anda masukkan adalah data yang benar dan bukan data orang lain. Keamanan dan kerahasiaan dokumen Anda terjamin.
                        </Text>

                        <Text style={[styles.txt2,{marginTop:24}]}>Nama Lengkap</Text>

                        <TextInput 
                        onChangeText={(fullName)=>this.setState({fullName})}

                        placeholder='Masukkan nama lengkap Anda' 
                        style={styles.input} />

                        <View style={styles.inline}>
                            <Text style={{ fontSize: 12 }}>Foto Selfie</Text>
                            <Text style={{ color: '#153E74',fontSize:12 }}>Lihat Contoh</Text>
                        </View>
                        <View style={styles.box}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('TryCamera2')}>
                                <Image source={require('../assets/images/camera1.png')} style={{ resizeMode: 'contain', width: 60, height: 50 }} />
                            </TouchableOpacity>
                            <Text style={{ marginTop: 14, fontSize: 12, color: '#153e74' }}>Ambil Foto</Text>
                        </View>
                        <View style={styles.inline}>
                            <Text style={{ fontSize: 12 }}>Foto Passport (Halaman Depan)</Text>
                            <Text style={{ color: '#153E74',fontSize:12 }}>Lihat Contoh</Text>
                        </View>
                        <View style={styles.box}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('TryCamera2')}>
                                <Image source={require('../assets/images/camera1.png')} style={{ resizeMode: 'contain', width: 60, height: 50 }} />
                            </TouchableOpacity>
                            <Text style={{ marginTop: 14, fontSize: 12, color: '#153e74' }}>Ambil Foto</Text>
                        </View>
                        <View style={styles.box3}>
                            <Text style={{fontSize:12, color:'#cccccc'}} >Nomor Passport</Text>
                        </View>
                        <View style={styles.inline}>
                            <Text style={{ fontSize: 12 }}>Foto Passport (Halaman Visa)</Text>
                            <Text style={{ color: '#153E74',fontSize:12 }}>Lihat Contoh</Text>
                        </View>
                        <View style={styles.box}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('TryCamera2')}>
                                <Image source={require('../assets/images/camera1.png')} style={{ resizeMode: 'contain', width: 60, height: 50 }} />
                            </TouchableOpacity>
                            <Text style={{ marginTop: 14, fontSize: 12, color: '#153e74' }}>Ambil Foto</Text>
                        </View>
                        <View style={[styles.box3,{marginBottom:20}]}>
                            <Text style={{fontSize:12, color:'#cccccc'}} >Nomor Visa</Text>
                        </View>
                        {/**/}
                        <Text style={styles.txt2}>Masa Berlaku Visa</Text>
                        <TextInput placeholder='Masukkan masa berlaku Visa' style={styles.input} />
                        <View style={styles.inline}>
                            <Text style={{ fontSize: 12 }}>Alamat Domisili</Text>
                        </View>
                        <View style={{width:'100%',borderRadius:2, borderWidth:1, borderColor:'#828282',height:80}}>
                            <TextInput placeholder='Masukkan alamat domisili Anda' style={{paddingLeft:14}}  />
                        </View>
                        <Text style={[styles.txt2,{marginTop:20}]}>Nama Kontak Darurat</Text>
                        <TextInput placeholder='Masukkan nama kontak darurat' style={styles.input} />
                        <Text style={[styles.txt2,{marginTop:20}]}>Nomor Kontak Darurat</Text>
                        <TextInput placeholder='Masukkan No. Kontak darurat' style={styles.input} />
                        <View style={styles.inline}>
                            <Text style={{ fontSize: 12 }}>Hubungan dengan Kontak Darurat</Text>
                        </View>
                        <View style={styles.boxModal}>
                            <Text style={{ fontSize: 14 }}>{this.state.pickerSelection}</Text>
                            <TouchableOpacity onPress={() => this.togglePicker()}>
                                <Image source={require('../assets/icons/down.png')} style={{ resizeMode: 'contain', height: 14, width: 16, marginRight:4 }} />
                            </TouchableOpacity>
                        </View>

                        <Modal visible={this.state.pickerDisplayed} transparent={true} animationType={'slide'} >
                            <View style={{ backgroundColor: '#000000aa', flex: 1}}>
                                <View style={{ backgroundColor: 'white', flex: 1,marginTop:'93%',height:50, width: '100%', borderRadius: 10, }}>
                                    <Text style={{ marginBottom: 8, fontSize: 15, fontWeight: 'bold', textAlign: 'center', alignItems: 'center' }}>Hubungan dengan kontak darurat:</Text>
                                    {pickerValues.map(
                                        (value, index) => {
                                            return (
                                                <TouchableHighlight key={index}  onPress={() => this.setPickerValue(value.value)} on style={{ paddingHorizontal: 4, alignItems: 'center' }}>
                                                    <Text style={{ fontSize: 15 }}>{value.title}</Text>
                                                </TouchableHighlight>
                                        )
                                    })}
                                </View>
                            </View>
                        </Modal>
                        <Text style={{ color: '#2D9CDB', fontSize: 12, lineHeight: 15, fontWeight: 'bold', marginTop: 20 }}>Periksa kembali ! Pastikan bahwa data yang Anda masukkan telah sesuai. </Text>
                            <View style={{ flexDirection: 'row', marginTop: 8, marginBottom:24 }}>
                                <CheckBox
                                    style={{borderWidth:2,
                                        borderColor:'#6e6b65', marginRight:11}}
                                    onClick={()=> {
                                        this.setState({isChecked:!this.state.isChecked})
                                    }}
                                    isChecked={this.state.isChecked}
                                />
                                    <Text>Data yang saya masukkan telah sesuai.</Text>
                            </View>
                    </ScrollView>

                <View style={styles.bottom}>
                    <TouchableOpacity
                        
                        onPress={()=>this.fungsiRegistrasi()}
                        style={styles.btn}>
                        <Text style={{color:'white',textAlign:'center',}}>Selanjutnya</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        )
    }

    fungsiRegistrasi =() => {
        this.props.navigation.navigate('Registrasi2')
        this.props.send(this.state.fullName)
    }

}
var addFullName = function(text){
    return{
        type: 'ADD_TEXT',
        data: text,
    }
}

const sendFullName=(dispatch)=>{
    return{
        send:(texts) => dispatch(addFullName(texts))
    }
}

export default connect(null,sendFullName)(Registrasi1);

const styles = StyleSheet.create({
    boxModal:{
        justifyContent:'space-between',
        flexDirection:'row',
        borderWidth: 1,
        borderColor: "black",
        borderRadius: 2,
        width: '100%',
        height: 46,
        paddingHorizontal: 14,
        paddingVertical: 14, 
    },
    inline:{
        marginTop:20,
        flexDirection:'row',marginTop:23, justifyContent:'space-between', marginBottom:8,
    },
    input:{
        width: '100%', 
        height:40,
        borderRadius:2 ,
        borderWidth:1,
        paddingLeft:14,
        borderColor:'#828282',
    },
    img:{
        resizeMode:'contain',
        width:260,
        height:20,
    },
    txt:{
        // marginTop:15,
        fontSize:16,
        color:'#153E74',
        fontWeight:'bold',
        lineHeight:20,
        textAlign:'center'
    },
    txt2:{
        fontSize: 12, 
        marginBottom: 8, 
        // marginTop: 24 
    },
    head:{ height: 56, backgroundColor: '#e5e5e5', justifyContent:'center', alignItems:'center', borderBottomWidth:1, borderColor:'#b7b5b5'},
    bottom:{height:90, backgroundColor:'#e5e5e5',paddingHorizontal:16, paddingVertical:22,},
    btn:{width:'100%',height:'100%',backgroundColor:'#153e74',justifyContent:'center', alignItems:'center'},
    box:{
        borderRadius:2,
        width:'100%',
        height:180, 
        borderColor:'#828282',
        borderWidth:1,
        alignItems:'center',
        justifyContent:'center',
    },
    box2:{
        borderTopLeftRadius:2,
        borderTopRightRadius:2,
        width:'100%',
        height:180, 
        borderColor:'black',
        borderWidth:1,
        alignItems:'center',
        justifyContent:'center',
    },
    box3:{
        height:40,
        width:'100%',
        backgroundColor:'#e0e0e0',
        borderBottomLeftRadius:2,
        borderBottomRightRadius:2,
        paddingLeft:14,
        paddingVertical:13,
    },
})
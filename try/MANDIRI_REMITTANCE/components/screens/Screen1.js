import React, { Component } from 'react';
import { View, Image,StyleSheet, TouchableOpacity } from 'react-native';

export default class Screen1 extends Component {
    render(){
        return(
            <TouchableOpacity 
                onPress={()=> this.props.navigation.navigate('Screen2')}
            style={styles.container}>
                <Image source={require('../assets/images/mandiriwhite.png')}  style={styles.img}/>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#153e74',
        justifyContent:'center',
        alignItems:'center'
    },
    img:{
        resizeMode:'contain',
        width:100,
        height:150,
    }
})
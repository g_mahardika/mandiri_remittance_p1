import {ADD_TEXT, SAVE_DATA} from './types';

export const addFullName= (text)=>{
    return{
        type: ADD_TEXT,
        data: text,
    }
}
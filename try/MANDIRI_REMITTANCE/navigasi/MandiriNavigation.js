import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import React from 'react';
import { StyleSheet } from 'react-native'

import Screen1 from '../components/screens/Screen1';
import Screen2 from '../components/screens/Screen2';
import Registrasi1 from '../components/screens/Registrasi1';
import TryCamera2 from '../components/screens/TryCamera2';
import Registrasi2 from '../components/screens/Registrasi2';

const Navigator = createStackNavigator({
    Screen1,
    Screen2,
    Registrasi1:{
        screen:Registrasi1, 
        navigationOptions:{
            headerShown:true,
            headerTitle:'Buka Rekening & Registrasi Akun',
    }},
    TryCamera2,
    Registrasi2,
},{
    initialRouteParams: 'MainTab',

    defaultNavigationOptions: {
        headerShown: false,
        headerStyle: {
            backgroundColor:'#e5e5e5'
        },
        headerTintColor:'black',
        headerTitleStyle:{
            fontSize:16
        }
    }
})



const MandiriNavigation = createAppContainer(Navigator);
export default MandiriNavigation;

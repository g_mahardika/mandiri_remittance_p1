import React from 'react';
import {Provider} from 'react-redux';
import store from './store/store';
import MandiriNavigation from './navigasi/MandiriNavigation';

function AppMandiri(){
    return(
        <Provider store={store}>
            <MandiriNavigation />
        </Provider>
    )
}

export default AppMandiri;
import {createStore, combineReducers} from 'redux';

import textReducer from '../reducer/textReducer';

const store = createStore(textReducer);

export default store;
import React, { Component } from 'react';
import { Image, ScrollView,SafeAreaView ,TouchableHighlight,StyleSheet, Modal,Text, TouchableOpacity, View, Picker } from 'react-native';
import { HeaderTabCommon, ButtonNextFooter } from '../../containers/atom'
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';



class AccountAndRegistration2 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pilihKerja: '',
            sumberPenghasilan: '',
            pickerString: 'Pilih jenis pekerjaan',
            pickerDisplayed: false,
            incomeString: 'Sumber Penghasilan',
            incomeDisplayed: false,
        }
    }

    setPickerValue(newValue) {
        this.setState({
            pickerString: newValue,
        })
        this.togglePicker();
    }

    togglePicker() {
        this.setState({ pickerDisplayed: !this.state.pickerDisplayed })
    }

    setIncomeValue(newValue) {
        this.setState({
            incomeString: newValue,
        })
        this.toggleIncome();
    }
    toggleIncome() {
        this.setState({ incomeDisplayed: !this.state.incomeDisplayed })
    }
    render(props) {
        const pickerValues = [
            { title: 'Karyawan Swasta', value: 'karyawan swasta' },
            { title: 'Wirausaha', value: 'wirausaha' },
            { title: 'Petani', value: 'petani' },
            { title: 'Nelayan', value: 'nelayan' },
            { title: 'Lainnya', value: 'lainnya' },
        ];
        const incomePicker = [
            { title: 'Karyawan', value: 'Karyawan' },
            { title: 'Menawarkan Barang & Jasa', value: 'Menawarkan Barang & Jasa' },
            { title: 'Wirausaha', value: 'Wirausaha' },
            { title: 'Investasi', value: 'Investasi' },
            { title: 'Lainnya', value: 'lainnya' },
        ];
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffffff' }}>
                {/*HEADER*/}

                <HeaderTabCommon navigation={this.props.navigation} title="Buka Rekening & Registrasi Akun" icn={require('../../assets/icons/backArrow.png')} />

                {/* BODY */}
                <View style={{ flex: 1, paddingHorizontal: 16, }}>
                    <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
                        <Text style={styles.txt}>Data Pekerjaan</Text>
                        <Text style={[styles.txt2, { color: '#828282', textAlign: 'center', marginTop: 8 }]}>
                            Pastikan data pekerjaan yang Anda masukkan adalah data yang benar.</Text>
                        <Text style={{ fontSize: 12, marginTop: 24 }}> Jenis Pekerjaan</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 16, justifyContent: 'space-between', marginTop: 8, width: '100%', borderColor: '#333333', borderWidth: 1, borderRadius: 2, height: 40 }}>

                            <Text style={{ fontSize: 13 }}>{this.state.pickerString}</Text>
                            <TouchableOpacity onPress={() => this.togglePicker()}>
                                <Image source={require('../../assets/icons/down.png')} style={{ resizeMode: 'contain', width: 16, marginRight: 14, height: 14 }} />
                            </TouchableOpacity>
                            <Modal visible={this.state.pickerDisplayed} transparent={true} animationType="slide" >

                                <View style={{ backgroundColor: '#000000aa', flex: 1, padding: 10 }}>
                                    <View style={{ backgroundColor: 'white', alignSelf: 'center', flex: 1, marginTop:hp ('30%'), width: 250, height:200,marginBottom:hp('32%'), borderRadius: 10, }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16, textAlign: 'center' }}>Jenis Pekerjaan : </Text>
                                        {pickerValues.map((value, index) => {
                                            return (
                                                <TouchableHighlight onPress={() => this.setPickerValue(value.title)}>
                                                    <Text style={{ fontSize: 14, marginLeft: 14, marginVertical: 10 }}>{value.title}</Text>
                                                </TouchableHighlight>
                                            )
                                        })}
                                    </View>
                                </View>

                            </Modal>
                        </View>

                        <Text style={{ fontSize: 12, marginTop: 24 }}> Sumber Penghasilan</Text>
                        <View style={{ flexDirection: 'row', marginTop: 8, width: '100%', borderColor: '#333333', borderWidth: 1, borderRadius: 2, height: 40, justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 14 }}>
                            <Text style={{ fontSize: 13 }}>{this.state.incomeString}</Text>
                            <TouchableOpacity onPress={() => this.setState({ incomeDisplayed: !this.state.pickerDisplayed })}>
                                <Image source={require('../../assets/icons/down.png')} style={{ resizeMode: 'contain', width: 16, height: 14 }} />
                            </TouchableOpacity>
                            <Modal visible={this.state.incomeDisplayed} transparent={true} animationType='slide' >
                                <View style={{ backgroundColor: '#000000aa', flex: 1, padding: 10, }}>
                                    <View style={{ backgroundColor: 'white', alignSelf: 'center', flex: 1, marginTop: hp('30%'), width: wp('67%'),height:200,marginBottom:hp('32%'), borderRadius: 10, }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16, textAlign: 'center' }}>Jenis Pendapatan : </Text>
                                        {incomePicker.map((value, index) => {
                                            return (
                                                <TouchableHighlight onPress={() => this.setIncomeValue(value.value)}>
                                                    <Text style={{ fontSize: 14, marginLeft: 14, marginVertical: 10 }}>{value.title}</Text>
                                                </TouchableHighlight>
                                            )
                                        })}
                                    </View>
                                </View>
                            </Modal>


                        </View>

                    </ScrollView>
                </View>


                <ButtonNextFooter navigation={this.props.navigation} txt="AccountAndRegistration3"/>
            </SafeAreaView>
        )
    }
}
export default AccountAndRegistration2

const styles = StyleSheet.create({
    headerTitleBox:{
        width:'100%',
        flexDirection:'row',
        paddingLeft:18,
        paddingVertical:18
    },
    arrow:{
        width:21,
        height:20,
        top:2,
        right:8,
    },
    headerTitle:{
        fontSize:16,
        lineHeight:20,
        width:268,
    },
    txt:{
        marginTop:15,
        fontSize:16,
        color:'#153E74',
        fontWeight:'bold',
        lineHeight:20,
        textAlign:'center'
    },
    txt2:{
        fontSize:12,
        lineHeight:15,
    },
    touch:{
        width:'100%',
        borderWidth:1,
        borderColor:'#BDBDBD',
        borderRadius:4,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        marginTop:14,
        paddingVertical:12,
        backgroundColor:"#153e74",
    },
    txt3:{
        marginBottom:10,
        fontSize:14,
        fontWeight:'bold', 
        alignItems:'center'
    }
})
import React,{Component} from 'react';
import { Image,SafeAreaView ,ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { HeaderTabCommon, ButtonNextFooter,TextAndInput, PasswordEye, Password } from '../../containers/atom';
import {KodePIN} from '../../containers/atom';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

class AccountAndRegistration5 extends Component{
    render(props){
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {/*HEADER*/}
                <HeaderTabCommon navigation={this.props.navigation} title="Buka Rekening & Registrasi Akun" icn={require('../../assets/icons/backArrow.png')} />

                {/* BODY */}
                <View style={{ flex: 1, paddingHorizontal: 16,backgroundColor:'#ffffff' }}>
                    <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
                        <Text style={styles.txt}>Verifikasi</Text>
                        <Text style={[styles.txt2, { color: '#828282', textAlign: 'center', marginTop: 8 }]}>
                            Kami akan mengirimkan kode OTP pada noomr ponsel yang Anda daftarkan sebelumnya
                        </Text>
                        <Text style={{ color: '#333333', fontWeight: 'bold', textAlign: 'center', height: 17, marginTop: 24 }}>0812-3423-8844</Text>
                        <KodePIN navigation={this.props.navigation} txt='AccountAndRegistration6' />
                        <Text style={{fontSize:11, color:'grey', textAlign:'center'}} >Hint: 123456</Text>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ marginTop: 40, fontSize: 12, }}>Belum menerima kode OTP ?</Text>
                            <Text style={{ fontWeight: 'bold', color: '#153e74' }}>Kirim ulang</Text>
                        </View>
                    </ScrollView>
                </View>

                {/* footer */}
                
                {/* <ButtonNextFooter navigation={this.props.navigation} txt="AccountAndRegistration6" /> */}
            </SafeAreaView>
        )
    }
}

export default AccountAndRegistration5

const styles = StyleSheet.create({
    headerTitleBox:{
        width:'100%',
        flexDirection:'row',
        paddingLeft:18,
        paddingVertical:18
    },
    arrow:{
        width:21,
        height:20,
        top:2,
        right:8,
    },
    headerTitle:{
        fontSize:16,
        lineHeight:20,
        width:268,
    },
    txt:{
        marginTop:15,
        fontSize:16,
        color:'#153E74',
        fontWeight:'bold',
        lineHeight:20,
        textAlign:'center'
    },
    txt2:{
        fontSize:12,
        lineHeight:15,
    },
    touch:{
        width:'100%',
        borderWidth:1,
        borderColor:'#BDBDBD',
        borderRadius:4,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        marginTop:14,
        paddingVertical:12,
        backgroundColor:"#153e74",
    },
    sixBox:{
        width:40, 
        marginRight:14,
        height:40, 
        borderColor:'#27ae60',
        borderWidth:1,
        borderRadius:2,
        justifyContent:'center', 
        alignContent:'center', 
        alignItems:'center',
        flex:1
    }
})

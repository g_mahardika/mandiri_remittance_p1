import React, { Component } from 'react';
import { Image,SafeAreaView ,ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { HeaderTabCommon, ButtonNextFooter } from '../../containers/atom'
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

class AccountAndRegistration3 extends Component {
    constructor(props){
        super(props)
        this.state={
            data:[
                {
                    id:0,
                    name:'Cabang 1',
                    address:'Wisma Mepro, Ground & Mezzanine Floor 29 - 31 Jalan Ipoh, 51200 Kuala Lumpur',
                    phone:'+603-4045-4988'
                },
                {
                    id:1,
                    name:'Cabang 2',
                    address:'Kedai Grapari Chowkit, 454 & 455, Tingkat Bawah Jalan Tuanku Abdul Rahman, Kuala Lumpur',
                    phone:'+603-2202-8163'
                },
                {
                    id:2,
                    name:'Cabang 3',
                    address:'Raya Surapati, Kompleks Surapati Core Office Block C No. 7, Bandung',
                    phone:'+6222-1234-5678'
                }
            ],
            checked:0,
        }
    }

    render(props) {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {/*HEADER*/}
                <HeaderTabCommon navigation={this.props.navigation} title="Buka Rekening & Registrasi Akun" icn={require('../../assets/icons/backArrow.png')} />
                
                {/* BODY */}
                <View style={{ flex: 1, paddingHorizontal: 16, backgroundColor: 'white' }}>
                    <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
                        <Text style={styles.txt}>Cabang Terdaftar</Text>
                        <Text style={[styles.txt2, { color: '#828282', textAlign: 'center', marginTop: 8 }]}>
                            Pilih kantor cabang sebagai tempat mendaftarkan rekening Mandiri International Remittance
                        </Text>
                        
                        <View style={{marginTop:20}}>
                            {
                                this.state.data.map((data,index)=>{
                                    return(
                                        <View style={{ marginBottom:12}}>
                                            {this.state.checked == index ?
                                                <TouchableOpacity style={{flexDirection:'row'}}>
                                                    <Image source={require('../../assets/icons/radioOn.png')} style={{width:26, height:25,marginRight:15}}/>
                                                    <View style={{flexDirection:'column'}}>
                                                        <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#333333' }}>{data.name}</Text>
                                                        <Text style={{alignSelf:'flex-start' ,marginTop: 8, color: '#4f4f4f', fontSize: 10 }}>{data.address}</Text>
                                                        <Text style={{ marginTop: 6, color: '#153e74', fontSize: 10, lineHeight: 12, fontWeight: 'bold' }}>{data.phone}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity style={{flexDirection:'row'}} onPress={()=>{this.setState({checked:index})}}>
                                                    <Image source={require('../../assets/icons/radioOff.png')} style={{width:26, height:25,marginRight:15}} />
                                                    <View style={{flexDirection:'column'}}>
                                                        <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#333333' }}>{data.name}</Text>
                                                        <Text style={{alignSelf:'flex-start', marginTop: 8, color: '#4f4f4f', fontSize: 10 }}>{data.address}</Text>
                                                        <Text style={{ marginTop: 6, color: '#153e74', fontSize: 10, lineHeight: 12, fontWeight: 'bold' }}>{data.phone}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            }
                                        </View>
                                    )
                                })
                            }
                        </View>

                    </ScrollView>
                </View>

                {/* FOOTER */}
                
                <ButtonNextFooter navigation={this.props.navigation} txt="AccountAndRegistration4" />
            </SafeAreaView>
        )
    }
}
// }
export default AccountAndRegistration3

const styles = StyleSheet.create({
    headerTitleBox:{
        width:'100%',
        flexDirection:'row',
        paddingLeft:18,
        paddingVertical:18
    },
    arrow:{
        width:21,
        height:20,
        top:2,
        right:8,
    },
    headerTitle:{
        fontSize:16,
        lineHeight:20,
        width:268,
    },
    txt:{
        marginTop:15,
        fontSize:16,
        color:'#153E74',
        fontWeight:'bold',
        lineHeight:20,
        textAlign:'center'
    },
    txt2:{
        fontSize:12,
        lineHeight:15,
    },
    touch:{
        width:'100%',
        borderWidth:1,
        borderColor:'#BDBDBD',
        borderRadius:4,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        marginTop:14,
        paddingVertical:12,
        backgroundColor:"#153e74",
    },
})
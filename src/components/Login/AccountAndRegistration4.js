import React, {Component} from 'react';
import { Image,SafeAreaView ,ScrollView, StyleSheet, Text, TouchableOpacity,TextInput, View } from 'react-native';

import { HeaderTabCommon, ButtonNextFooter,TextAndInput, PasswordEye, Password } from '../../containers/atom'
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';


class AccountAndRegistration4 extends Component{
    constructor(props){
        super(props)
        this.state={
            security:true,
            password:'',
        }
    }
    render(props){
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {/*HEADER*/}
                <HeaderTabCommon navigation={this.props.navigation} title="Buka Rekening & Registrasi Akun" icn={require('../../assets/icons/backArrow.png')} />

                {/* BODY */}
                <View style={{ flex: 1, paddingHorizontal: 16, backgroundColor:'#ffffff'}}>
                    <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
                        <Text style={styles.txt}>Akun Aplikasi</Text>
                        <Text style={[styles.txt2, { color: '#828282', textAlign: 'center', marginTop: 8 }]}>
                            Data ini akan Anda gunakan untuk mengakses aplikasi Mandiri International Remittance
                        </Text>
                        <TextAndInput txt="Nomor Ponsel" placeholder="Masukkan nomor ponselAnda"/>
                        <TextAndInput txt="Email" placeholder="Masukkan alamat email lAnda"/>
                        <PasswordEye text1="Kata Sandi" txt2="Maskukkan kata sandi Anda" />
                        <Text style={{ marginTop: 8, color: '#153e74', fontSize: 10 }}>* Kata sandi terdiri dari 6 - 10 digit</Text>
                        <Password text1="Ulangi Kata Sandi" text2="Ulangi kata sandi Anda" />
                        
                    </ScrollView>
                </View>

                {/* footer */}
                <ButtonNextFooter navigation={this.props.navigation} txt="AccountAndRegistration5" />
            </SafeAreaView>
        )
    }
}

export default AccountAndRegistration4

const styles = StyleSheet.create({
    headerTitleBox:{
        width:'100%',
        flexDirection:'row',
        paddingLeft:18,
        paddingVertical:18
    },
    arrow:{
        width:21,
        height:20,
        top:2,
        right:8,
    },
    headerTitle:{
        fontSize:16,
        lineHeight:20,
        width:268,
    },
    txt:{
        marginTop:15,
        fontSize:16,
        color:'#153E74',
        fontWeight:'bold',
        lineHeight:20,
        textAlign:'center'
    },
    txt2:{
        fontSize:12,
        lineHeight:15,
    },
    touch:{
        width:'100%',
        borderWidth:1,
        borderColor:'#BDBDBD',
        borderRadius:4,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        marginTop:14,
        paddingVertical:12,
        backgroundColor:"#153e74",
    },
})

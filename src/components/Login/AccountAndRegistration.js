import React, { Component } from 'react';
import { Image, ScrollView ,StyleSheet, Text,SafeAreaView, TouchableOpacity, View, TextInput,Picker,Modal, TouchableHighlight, Button } from 'react-native';
import {BoxWordsImage} from '../../containers/organisms';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box';
import { 
    HeaderTab, TextAndInput,
    ButtonNextFooter,
} from '../../containers/atom';


export default class AccountAndRegistration extends Component {
    constructor(props){
        super(props)
        this.state={
            pilihHubungan:'',
            isChecked: false,
            pickerSelection:'Pilih Hubungan :',
            pickerDisplayed:false,
        }
    }

    setPickerValue(newValue){
        this.setState({
            pickerSelection:newValue
        });
        this.togglePicker();
    }

    togglePicker(){
        this.setState({
            pickerDisplayed:!this.state.pickerDisplayed
        })
    }
   
    render(){
        const pickerValues= [
            {title:'Ayah', value:'Ayah',},
            {title:'Ibu', value:'Ibu',},
            {title:'Saudara Laki-laki', value:'Saudara Laki-laki',},
            {title:'Saudara Perempuan', value:'Saudara Perempuan',}
        ]
        return (
            <SafeAreaView style={{flex:1}}>
                <HeaderTab txt="Greeting" navigation={this.props.navigation} title="Buka Rekening & Registrasi Akun" icn={require('../../assets/icons/backArrow.png')}  />
                {/* data */}
                <View style={{ flex: 1, paddingHorizontal: 16,backgroundColor:'#ffffff' }}>
                    <ScrollView style={{flex:1}}>
                        <Text style={styles.txt}>Data & Dokumen Pribadi </Text>
                        <Text style={[styles.txt2, { color: '#828282', textAlign: 'center', marginTop: 8 }]}>
                            Pastikan data yang Anda masukkan adalah data yang benar dan bukan data orang lain. Keamanan dan kerahasiaan dokumen Anda terjamin.
                        </Text>
                        <TextAndInput txt="Nama Lengkap" placeholder="Masukkan nama lengkap Anda" />
                        <BoxWordsImage navigation={this.props.navigation} />
                        <TextAndInput txt="Masa Berlaku Visa" placeholder="Masukkan masa berlaku visa Anda"/>
                        <View style={{ marginTop: 20 }}>
                            <Text>Alamat Domisili</Text>
                            <View style={styles.boxAlamat}>
                                {/* <Text style={{ color: "grey" }}>Masukkan alamat domisili</Text> */}
                                <TextInput placeholder="Masukkan alamat domisili."/>
                            </View>
                        </View>
                        <TextAndInput txt="Nama Kontak Darurat" placeholder="Masukkan Nama Kontak Darurat"/>
                        <TextAndInput txt="Nomor Kontak Darurat" placeholder="Masukkan Nomor Kontak Darurat"/>
                        <Text style={{ marginTop: 20, fontSize: 12 }}>Hubungan dengan Kontak Darurat </Text>
                        <View style={[styles.box, { marginTop: 8, justifyContent: 'space-between', flexDirection:'row',}]}>
                            <Text style={{fontSize:14}}>{this.state.pickerSelection}</Text>
                            <TouchableOpacity onPress={()=>this.togglePicker()}>
                                <Image source={require('../../assets/icons/down.png')} style={{resizeMode:'contain',height:14,width:16}} />
                            </TouchableOpacity>
                        </View>
                        <Modal visible={this.state.pickerDisplayed} transparent={true} animationType={'slide'} >
                            <View style={{ backgroundColor: '#000000aa', flex: 1, padding: 10,paddingBottom: hp('10%'),}}>
                                <View style={{ backgroundColor: 'white',height:0, alignSelf: 'center', flex: 1, marginTop: hp('70%'), width: wp ('75%'), borderRadius: 10, }}>
                                    <Text style={{ marginBottom: 10, fontSize: 15, fontWeight: 'bold', textAlign: 'center',alignItems:'center' }}>Hubungan dengan kontak darurat:</Text>
                                    { pickerValues.map((value, index) => {
                                        return (
                                            <TouchableHighlight onPress={() => this.setPickerValue(value.value,key=index)} on style={{ paddingHorizontal: 4, alignItems: 'center' }}>
                                                <Text style={{ fontSize: 15 }}>{value.title}</Text>
                                            </TouchableHighlight>
                                        )
                                    })}
                                </View>
                            </View>
                        </Modal>
                        <Text style={{ color: '#2D9CDB', fontSize: 12, lineHeight: 15, fontWeight: 'bold', marginTop: 20 }}>Periksa kembali ! Pastikan bahwa data yang Anda masukkan telah sesuai. </Text>
                        <View style={{ flexDirection: 'row', marginTop: 8 }}>
                            
                            <CheckBox
                                style={{borderWidth:2,
                                    borderColor:'#6e6b65', marginRight:11}}
                                onClick={()=> {
                                    this.setState({isChecked:!this.state.isChecked})
                                }}
                                isChecked={this.state.isChecked}
                            />
                                <Text>Data yang saya masukkan telah sesuai.</Text>
                        </View>

                    </ScrollView>
                </View>
                <ButtonNextFooter navigation={this.props.navigation} txt="AccountAndRegistration2"/>
            </SafeAreaView>
        )
    }
}

const styles= StyleSheet.create({
    headerTitleBox:{
        width:'100%',
        flexDirection:'row',
        paddingLeft:18,
        paddingVertical:18
    },
    arrow:{
        width:21,
        height:20,
        top:2,
        right:8,
    },
    headerTitle:{
        fontSize:16,
        lineHeight:20,
        width:268,
    },
    txt:{
        marginTop:15,
        fontSize:16,
        color:'#153E74',
        fontWeight:'bold',
        lineHeight:20,
        textAlign:'center'
    },
    txt2:{
        fontSize:12,
        lineHeight:15,
    },
    touch:{
        width:'100%',
        borderWidth:1,
        borderColor:'#BDBDBD',
        borderRadius:4,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        marginTop:14,
        paddingVertical:12,
        backgroundColor:"#153e74"
    },
    box:{
        marginTop: 8,
        borderWidth: 1,
        borderColor: "black",
        borderRadius: 2,
        width: '100%',
        height: 46,
        paddingHorizontal: 5,
        paddingVertical: 14, 
        // backgroundColor:'green',
    },
    boxAlamat:{
        marginTop:8,
        borderWidth:1,
        borderColor:"black",
        borderRadius:2,
        width:'100%',
        height:80,
        paddingLeft:14,
        paddingVertical:12, 
    },
    miniBox:{
        borderWidth:2,
        borderColor:'#6e6b65',
        height:18,
        width:18,
        marginRight:11,
    }
})
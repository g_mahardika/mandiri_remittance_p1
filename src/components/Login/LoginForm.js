import React, { Component } from 'react';
import { Image, StyleSheet,SafeAreaView ,Text, TouchableOpacity, View, TextInput,Button, ScrollView } from 'react-native';
import {HeaderTab, TextAndInput, PasswordEye, BottomBlue} from '../../containers/atom'


class LoginForm extends Component{
    constructor(props){
        super(props)
        this.state={
            security:true,
        }
    }
    render(props){
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
                <HeaderTab txt='Greeting' navigation={this.props.navigation} title="" icn={require('../../assets/icons/backArrow.png')} />
                <View style={{ marginTop: 0, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/images/MandiriLogo.png')} style={styles.logo} />
                    <Text style={{ marginTop: 24, fontSize: 16, color: '#153e74' }}>Selamat Datang Remitter</Text>
                </View> 
                <View style={{ borderWidth: 1, borderColor: '#e0e0e0', width: '100%', marginTop: 14 }} />
                    <ScrollView>
                        <View style={{ marginTop: 4, paddingHorizontal: 16 }}>
                            <TextAndInput txt="Email" placeholder="Masukkan alamat emaiil Anda" />
                            <PasswordEye text1='Kata Sandi' txt2='Masukkan kata sandi Anda' />
                            <View style={{ marginTop: 14, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                <Text style={{ fontSize: 12 }}>Lupa kata sandi?</Text>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('ResetPassword')}
                                >
                                    <Text style={{ color: '#153e74', fontSize: 12, fontWeight: 'bold' }}>Reset</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ marginTop: 8 }}>
                                <BottomBlue navigation={this.props.navigation} txt='Login' txt2='MainHome' style={{ marginTop: 20 }} /></View>
                            
                            <TouchableOpacity style={styles.txtBx2} onPress={() => this.props.navigation.navigate('TryFingerprint')}>
                                <Image source={require('../../assets/images/fingerprint.png')} style={{ width: 23, height: 23 }} />
                                <Text style={{ color: '#153e74', fontSize: 14, marginLeft: 8 }}>Masuk dengan Sidik Jari</Text>
                            </TouchableOpacity>
                            <View style={{ marginTop: 54, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                                <Text style={{ fontSize: 12 }}>Anda belum punya rekening?</Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('AccountAndRegistration')}>
                                    <Text style={{ color: '#153e74', fontWeight: 'bold' }}> Buat Rekening</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
            </SafeAreaView>
        )
    }
}

export default LoginForm;

const styles=StyleSheet.create({
    logo:{
        marginTop:8,
        width:160, 
        height:80,
        
    },
    txtBx:{
        marginTop:8, 
        flexDirection:'row', 
        height:40, 
        borderWidth:1, 
        borderColor:'#828282', 
        paddingHorizontal:14, 
        paddingVertical:0, 
        // justifyContent:'space-between',
    },
    txtBx2:{
        marginTop:14, 
        flexDirection:'row', 
        height:45, 
        borderWidth:1, 
        borderColor:'#153e74', 
        paddingHorizontal:14, 
        paddingVertical:12, 
        justifyContent:'center',
        alignItems:'center',
    }
})
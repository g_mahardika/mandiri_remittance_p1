import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, SafeAreaView } from 'react-native';
import { BottomWhite } from '../../containers/atom';

class AccountAndRegistration6 extends Component{
    render(props){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'white'}}>
                <View style={styles.body}>
                    <Image source={require('../../assets/images/success.png')}
                        style={{ width: 130, height: 130, }}
                    />
                    <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#153e74', marginTop: 24 }}> Selesai</Text>
                    <Text style={{ textAlign: 'center', marginTop: 8, color: '#828282' }}>Pembuatan Rekening dan Akun Mandiri International Remittance berhasil dibuat.</Text>
                </View>
                <View style={{ width: '100%', height: 55, paddingHorizontal: 16, backgroundColor: 'white' }}>
                    <BottomWhite navigation={this.props.navigation} txt="Login Sekarang" txt2='LoginForm'/>
                </View>
            </SafeAreaView>
        )
    }
}
export default AccountAndRegistration6;

const styles=StyleSheet.create({
    body:{
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
        paddingHorizontal:16,
        flex:1,
    },
    touch:{
        width:'100%',
        borderWidth:1,
        borderColor:'#153e74',
        borderRadius:4,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        marginTop:14,
        paddingVertical:12,
       
    },
})
import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView} from 'react-native';
import { ScrollableProduct } from '../containers/organisms';
import {BottomBlue, BottomWhite} from '../containers/atom';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export default class Greeting extends Component {
    render(){
        return(
            <SafeAreaView style={{flex:1}}>
                <ScrollableProduct style={{flex:1}} />
                <View style={styles.bottomLine}>
                    <BottomWhite txt="Buka Rekening Sekarang" txt2='AccountAndRegistration' navigation={this.props.navigation} />
                    <BottomBlue txt="Login" txt2='FoodForm' navigation={this.props.navigation} />
                </View>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    bottomLine:{
        // height:hp ('15%'),
        // backgroundColor:'green',
        height:100,
        marginHorizontal:16,
    }
})
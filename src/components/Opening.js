import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView} from 'react-native';

export default class Opening extends Component{
    render(){
        return(
                <TouchableOpacity
                    onPress={()=> this.props.navigation.navigate('Greeting')}
                    style={styles.screen}
                >
                    <SafeAreaView >
                        <Image source={require('../assets/images/mandiriwhite.png')} style={styles.img} />
                    </SafeAreaView>
                </TouchableOpacity>
        )
    }
}

const styles=StyleSheet.create({
    screen:{
        backgroundColor:'#2a569c',
        justifyContent:'center',
        alignItems:'center',
        flex:1,
    },
    img:{
        width:160,
        height:80
    }
})
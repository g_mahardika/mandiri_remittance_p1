import React, { Component } from 'react';
import {View,Text,StyleSheet,SafeAreaView, Image, TextInput, TouchableOpacity,Modal, ScrollView } from 'react-native';
import {HeaderTabCommon, BottomBlue, KodePIN,} from '../../containers/atom'
import { heightPercentageToDP as hp} from 'react-native-responsive-screen';


export default class AkunKonfirmasiPIN extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false,
        }
    }
    render(props){
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor:'#ffffff' }}>

                <HeaderTabCommon title="" navigation={this.props.navigation} />
                <ScrollView >
                    <View style={{flex:1, marginBottom:hp('47%')}}>
                    <View style={{ marginTop: 10, marginHorizontal: 20, }}>
                        <Text style={styles.txt}>Konfirmasi PIN</Text>
                        <Text style={{ marginTop: 24, textAlign: 'center' }}>Ulangi PIN baru Anda.</Text>
                    </View>
                    <View style={{padding:20}}>
                        <KodePIN txt='' navigation={this.props.navigation} />
                        <Text style={{color:'gray', fontSize:10}}>Hint: 123456</Text>
                    </View>
                    </View>

                    <View style={{height:60,padding:18}}>
                    <TouchableOpacity onPress={()=>this.setState({show:!this.state.show})}>
                        <View style={{ width: '100%', justifyContent: 'center', height: 45, borderWidth: 1, borderColor: '#828282', borderRadius: 4, paddingHorizontal: 16, backgroundColor: '#153e74' }}>
                            <Text style={{ color: '#ffffff', fontSize: 16, textAlign: 'center', }}>Lanjut</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <Modal visible={this.state.show} transparent={this.state.show} animationType="slide">
                    <View style={styles.modalContent}>
                        <View style={{ backgroundColor: 'white',width:280, height:260,borderRadius: 10, }}>
                            <Text style={{textAlign:'center',fontSize:14, color:'#153e74',marginTop:20}}>PIN baru Anda telah terbuat</Text>
                            <View style={{justifyContent:'center', alignItems:'center'}}>
                                <Image style={{ marginTop: 37, width: 130, height: 130 }} source={require('../../assets/images/success.png')} />
                                <TouchableOpacity
                                    onPress={() => this.setState({ show: false })}
                                >
                                    <Text style={{fontSize:15, color:'#153e74', marginTop:10}}>Selesai</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
                </ScrollView>


            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    body:{
        flex:1,
    },
    modalContent:{
        backgroundColor:'#000000aa',
        padding:10,
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
    },
    txt:{
        textAlign:'center'
    }
})
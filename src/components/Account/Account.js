import React, { Component} from 'react';
import {ScrollView ,Image, Modal, StyleSheet, Text, TouchableOpacity, View, SafeAreaView } from 'react-native';

import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import AkunInitial from '../../containers/molecules/AkunInitial';


export default class Akun extends Component {
    render(){
        return(
            <SafeAreaView style={{flex:1}}>
                <ScrollView style={{flex:1}}>
                    <View style={{ height: 56, padding: 18 }}>
                        <Text style={{ fontSize: 16 }}>Akun</Text>
                    </View>
                    <Image style={{ marginTop: 0, height: 60, width: 60, alignSelf: 'center' }} source={require('../../assets/images/foto.png')} />
                    <Text style={{ color: '#153e74', marginTop:hp('2.2%'), fontSize: 16, fontWeight: 'bold', textAlign: 'center' }}>Mayuga Wicaksana</Text>
                    <Text style={{ marginTop:hp('2%'), fontSize: 12, textAlign: 'center' }}>mayugawicaksana18@gmail.com</Text>
                    <Text style={{ marginTop: hp('1%'), fontSize: 12, textAlign: 'center' }}>0896-6289-0769</Text>
                    <View style={{ marginVertical:hp('2.5%'), width: '100%', height: 4, backgroundColor: '#e0e0e0' }} />
                    <View style={{marginHorizontal:16, backgroundColor:'', marginBottom:hp('1.6%')}}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Pengaturan</Text>
                        <AkunInitial 
                        navigation={this.props.navigation}
                            txt1='Ubah kata sandi'
                            txt2='AkunUbahSandi'
                            img={require('../../assets/icons/katasandiicon.png')} 
                            img2={require('../../assets/icons/right.png')}/>
                        <View style={{ marginTop: 5, marginLeft: wp('5%'), width: wp('80%'), height: 2, backgroundColor: '#e0e0e0' }} />
                        <AkunInitial 
                            navigation={this.props.navigation}
                            txt1='Ubah PIN' 
                            txt2='AkunUbahPIN'
                            img={require('../../assets/icons/pinicon.png')} 
                            img2={require('../../assets/icons/right.png')}/>
                    </View>
                    <View style={{ marginBottom:hp('2%') , width: wp('100%'), height: 4, backgroundColor: '#e0e0e0' }} />
                    <View style={{marginHorizontal:16, backgroundColor:'', marginBottom:hp('2%')}}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Informasi</Text>
                        <AkunInitial 
                            navigation={this.props.navigation}
                            txt1='Tentang Aplikasi ' 
                            txt2='TentangAplikasi'
                            img={require('../../assets/icons/tentangicon.png')} 
                            img2={require('../../assets/icons/right.png')}/>
                        <View style={{ marginTop: 5, marginLeft: wp('5%'), width: wp('80%'), height: 2, backgroundColor: '#e0e0e0' }} />
                        <AkunInitial 
                            navigation={this.props.navigation}
                            txt1='Hubungi kami' 
                            txt2='HubungiKami'
                            img={require('../../assets/icons/hubungiicon.png')} 
                            img2={require('../../assets/icons/right.png')}/>
                        <View style={{ marginTop: 5, marginLeft: wp('5%'), width: wp('80%'), height: 2, backgroundColor: '#e0e0e0' }} />

                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('Opening')}>

                            <AkunInitial
                                navigation={this.props.navigation}
                                txt1='Keluar'
                                // txt2='Opening'
                                img={require('../../assets/icons/logout-2.png')}
                            // img2={require('../../assets/icons/right.png')}
                            />
                        </TouchableOpacity>
                    </View>
                    
                </ScrollView>
            </SafeAreaView>
        )
    }
}  
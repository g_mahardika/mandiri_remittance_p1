import React, { Component } from 'react';
import {SafeAreaView,Text,StyleSheet, Image,View, TextInput, TouchableOpacity,Modal, ScrollView } from 'react-native';
// import {HeadBackArrow} from '../../../components/atoms';
import {HeaderTabCommon} from '../../containers/atom';


export default class HubungiKami extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false,
        }
    }
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#ffffff'}} >
                <View style={{flex:1, }}>

                   <HeaderTabCommon title='Kontak Perusahaan' navigation={this.props.navigation } />
                    
                    <Text style={{marginBottom:24,textAlign:'center',marginTop:40,marginHorizontal:16,color:'#153e74'}}>Kunjungi media sosial Mandiri International Remittance untuk mendapatkan informasi menarik lain tentang kami</Text>
                <View style={{height:2,borderWidth:1,borderColor:'#e0e0e0', width:'100%'}} />
                {/* <View style={{marginTop:40}}>
                </View> */}
                    <View style={{ marginBottom: 24, marginTop:20 }}>
                        {/* <Text style={{ fontSize: 12, fontWeight: 'bold', marginBottom: 23 }}>Pengaturan</Text> */}
                        <View style={{ flexDirection: 'row', borderRadius: 3, borderWidth: 0, width: '100%', height: 40, paddingHorizontal: 14 }} >
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <Image source={require('../../assets/images/callcenter.png')} style={{ marginRight: 14, resizeMode: 'contain', width: 26, marginTop: 7, height: 26 }} />
                                <Text style={{ fontSize: 13, marginTop: 9 }}>Call Center</Text>
                            </View>
                            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('AkunMandiriUbah')}>
                                <Image source={require('../../assets/icons/right.png')} style={{ resizeMode: 'contain', width: 20, marginTop: 10, height: 14 }} />
                            </TouchableOpacity> */}
                        </View>
                        <View style={{ marginTop: 1, borderColor: '#e0e0e0', borderWidth: 1, marginLeft: '14%', width: '80.5%' }} />
                        {/*  */}
                        <View style={{ flexDirection: 'row', borderRadius: 3, marginTop: 16, borderWidth: 0, width: '100%', height: 40, paddingHorizontal: 14 }} >
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <Image source={require('../../assets/images/website.png')} style={{ marginRight: 14, resizeMode: 'contain', width: 26, marginTop: 7, height: 26 }} />
                                <Text style={{ fontSize: 13, marginTop: 9 }}>Our Website</Text>
                            </View>
                            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('AkunMandiriUbahPIN')}>
                                <Image source={require('../../assets/icons/right.png')} style={{ resizeMode: 'contain', width: 20, marginTop: 10, height: 14 }} />
                            </TouchableOpacity> */}
                        </View>
                        <View style={{ marginTop: 1, borderColor: '#e0e0e0', borderWidth: 1, marginLeft: '14%', width: '80.5%' }} />
                        {/*  */}
                        <View style={{ flexDirection: 'row', borderRadius: 3,marginTop:16, borderWidth: 0, width: '100%', height: 40, paddingHorizontal: 14 }} >
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <Image source={require('../../assets/images/instagram.png')} style={{ marginRight: 14, resizeMode: 'contain', width: 26, marginTop: 7, height: 26 }} />
                                <Text style={{ fontSize: 13, marginTop: 9 }}>Instagram</Text>
                            </View>
                            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('AkunMandiriUbah')}>
                                <Image source={require('../../assets/icons/right.png')} style={{ resizeMode: 'contain', width: 20, marginTop: 10, height: 14 }} />
                            </TouchableOpacity> */}
                        </View>
                        <View style={{ marginTop: 1, borderColor: '#e0e0e0', borderWidth: 1, marginLeft: '14%', width: '80.5%' }} />
                        {/*  */}
                        <View style={{ flexDirection: 'row', borderRadius: 3, marginTop: 16, borderWidth: 0, width: '100%', height: 40, paddingHorizontal: 14 }} >
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <Image source={require('../../assets/images/facebook.png')} style={{ marginRight: 14, resizeMode: 'contain', width: 26, marginTop: 7, height: 26 }} />
                                <Text style={{ fontSize: 13, marginTop: 9 }}>Facebbok</Text>
                            </View>
                            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('AkunMandiriUbahPIN')}>
                                <Image source={require('../../assets/icons/right.png')} style={{ resizeMode: 'contain', width: 20, marginTop: 10, height: 14 }} />
                            </TouchableOpacity> */}
                        </View>
                        <View style={{ marginTop: 1, borderColor: '#e0e0e0', borderWidth: 1, marginLeft: '14%', width: '80.5%' }} />
                        {/*  */}
                    </View>

                </View>

            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    headerTitleBox: {
        width: '100%',
        flexDirection: 'row',
        // paddingLeft: 18,
        paddingVertical: 18
    },
    arrow:{
        width:21,
        height:20,
        top:2,
        marginRight:14,
    },

})
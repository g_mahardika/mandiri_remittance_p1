import React, { Component } from 'react';
import { Image,SafeAreaView, StyleSheet,Text, TextInput, TouchableOpacity, View, Modal, ScrollView } from 'react-native';
import {HeaderTabCommon, BottomBlue }from '../../containers/atom'
import { heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class AkunUbahSandi extends Component{
    constructor(props){
        super(props)
        this.state={
            security: true,
            show:false,
        }
    }
    render(props){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'white'}}>
                <HeaderTabCommon navigation={this.props.navigation} title="Ubah Kata Sandi" />
                <ScrollView >
                <View style={{flex:1, marginBottom:hp('27%')}}>
                        <View style={{ marginHorizontal: 16 }}>
                            <Text style={{ marginTop: 24, fontSize: 13 }} >Kata Sandi</Text>
                            <View style={{ flexDirection: 'row', marginTop: 8, width: '100%', height: 40, borderWidth: 1, borderRadius: 3, borderColor: '#828282' }} >
                                <TextInput placeholder="kata sandi" secureTextEntry={this.state.security} style={{ paddingLeft: 16, flex: 1 }} />
                                <View style={{ width: 30 }}>
                                    <TouchableOpacity onPress={() => this.setState({ security: !this.state.security })}>
                                        <Image source={require('../../assets/icons/eye.png')} style={{ resizeMode: 'contain', width: 20, height: 24, marginTop: 7, marginRight: 10 }} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Text style={{ fontSize: 10, marginTop: 8, color: '#153e74' }}>* Kata sandi terdiri dari 6 - 10 digit</Text>

                            <View style={{ marginTop: 20 }}>
                                <Text style={{ fontSize: 13 }}>Ulangi Kata Sandi </Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 8, width: '100%', height: 40, borderWidth: 1, borderRadius: 3, borderColor: '#828282' }} >
                                    <TextInput placeholder="kata sandi" secureTextEntry={this.state.security} style={{ paddingLeft: 16, flex: 1 }} />
                                    {/* <Image source={require('../../../assets/icons/eye.png')} style={{ resizeMode: 'contain', width: 20, height: 24, marginTop: 7, marginRight: 10 }} /> */}
                                </View>
                            </View>
                            <View style={{ marginTop: 53, alignSelf: 'center' }}>
                                <Image source={require('../../assets/images/locker.png')} style={{ width: 198, height: 176, }} />
                            </View>
                        </View>
                </View>
                    
                
                <View style={{height:80,padding:18}}>
                    <TouchableOpacity onPress={()=>this.setState({show:!this.state.show})}>
                        <View style={{ width: '100%', justifyContent: 'center', height: 45, borderWidth: 1, borderColor: '#828282', borderRadius: 4, paddingHorizontal: 16, backgroundColor: '#153e74' }}>
                            <Text style={{ color: '#ffffff', fontSize: 16, textAlign: 'center', }}>Ubah Kata Sandi</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <Modal visible={this.state.show} transparent={this.state.show} animationType="slide">
                    <View style={styles.modalContent}>
                        <View style={{ backgroundColor: 'white',width:280, height:260,borderRadius: 10, }}>
                            <Text style={{textAlign:'center',fontSize:14, color:'#153e74',marginTop:20}}>Kata sandi Anda berhasil diubah.</Text>
                            <View style={{justifyContent:'center', alignItems:'center'}}>
                                <Image style={{ marginTop: 37, width: 130, height: 130 }} source={require('../../assets/images/success.png')} />
                                <TouchableOpacity
                                    onPress={() => this.setState({ show: false })}
                                >
                                    <Text style={{fontSize:15, color:'#153e74', marginTop:10}}>Selesai</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
               </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    body:{
        flex:1,
    },
    modalContent:{
        backgroundColor:'#000000aa',
        padding:10,
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
    }
})
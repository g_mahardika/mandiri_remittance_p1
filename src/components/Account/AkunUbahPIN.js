import React, { Component } from 'react';
import {View,Text,StyleSheet,SafeAreaView, Image, TextInput, TouchableOpacity,Modal, ScrollView } from 'react-native';
import {HeaderTabCommon, BottomBlue, KodePIN,} from '../../containers/atom'

export default class AkunUbahPIN extends Component{
    render(props){
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor:'#ffffff' }}>
                <HeaderTabCommon title="" navigation={this.props.navigation} />
                <ScrollView style={{flex:1}}>
                    <View style={{ marginTop: 10, marginHorizontal: 20, }}>
                        <Text style={styles.txt}>PIN Lama</Text>
                        <Text style={{ marginTop: 24, textAlign: 'center' }}>Untuk melakukan perubahan PIN lama, masukkan PIN Anda sebelumnya</Text>
                    </View>
                    <View style={{padding:20}}>
                        <KodePIN txt='AkunPINBaru' navigation={this.props.navigation} />
                        <Text style={{color:'gray', fontSize:10}}>Hint: 123456</Text>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    txt:{
        fontSize:20,
        color:'#153e74',
        textAlign:'center',   
    },
    input:{
        
    }
})
import React, { Component } from 'react';
import {SafeAreaView,View,Text,StyleSheet, Image, TextInput, TouchableOpacity,Modal, ScrollView } from 'react-native';
import {HeaderTabCommon} from '../../containers/atom';

export default class TentangAplikasi extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false,
        }
    }
    render(){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#ffffff'}} >
                <HeaderTabCommon title="Tentang Aplikasi" navigation={this.props.navigation} />
                <ScrollView style={{flex:1, }}>
                    
                    <Image source={require('../../assets/images/MandiriLogo.png')} style={{marginBottom:58,resizeMode:'contain',width:160, height:80,marginTop:20, alignItems:'center',marginLeft:'26%'}} />
                    <View style={{height:1,borderWidth:1,borderColor:'#e0e0e0', width:'100%'}} />
                    <View style={{marginTop:22, marginBottom:46,paddingHorizontal:16}}>
                        <Text style={{marginBottom:60}}>Id in mollit incididunt irure incididunt aute magna nisi elit occaecat amet sunt. Id irure sunt in eu irure aute commodo fugiat amet. Esse exercitation eiusmod eu nisi tempor veniam culpa exercitation sit. Eu cillum occaecat anim cillum non commodo anim sunt mollit tempor.</Text>
                        <Text style={{color:'#153e74',marginBottom:14}}>Mandiri Remittance Mobile Hak Cipta Terlindungi</Text>
                        <Text style={{marginBottom:70}}>Untuk informasi lebih lanjut, kunjungi kami http://www.mandiriremittance.com atau hubungi kami di *** ***</Text>
                        <Text >Copyright 2020 -</Text>
                        <Text>Mandiri International Remittance Shn Bhd</Text>
                    </View>
                </ScrollView>


            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    headerTitleBox: {
        width: '100%',
        flexDirection: 'row',
        // paddingLeft: 18,
        paddingVertical: 18
    },
    arrow:{
        width:21,
        height:20,
        top:2,
        marginRight:14,
    },

})
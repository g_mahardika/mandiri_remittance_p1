import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity,SafeAreaView, View, TextInput } from 'react-native';
import {MainHomeHead} from '../../containers/molecules';
import {MainHomeBody, MainHomeFooter} from '../../containers/organisms';

class MainHome extends Component{
    constructor(props){
        super(props)
        this.state={
            color:'black',
        }
    }

    render(props){
        return(
            <SafeAreaView style={{flex:1,paddingTop:0, backgroundColor:'#ffffff'}}>
                <MainHomeHead />
                <MainHomeBody 
                navigation={this.props.navigation}
                />
            </SafeAreaView>
        )
    } 
}

export default MainHome;

const styles = StyleSheet.create({
    container:{
        justifyContent:'space-between',
        alignContent:'center',
        alignItems:'center',
        flexDirection:'row',
        flex:1,
    },
    img:{
        width:20,
        height:20,
    }
})
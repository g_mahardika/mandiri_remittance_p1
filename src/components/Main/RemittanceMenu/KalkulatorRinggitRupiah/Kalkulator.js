import React, { Component } from 'react';
import { SafeAreaView,Image, StyleSheet,Text, TextInput, TouchableOpacity, View, Modal, ScrollView } from 'react-native';
import {HeaderTabCommon} from '../../../../containers/atom'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';


export default class Kalkulator extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false
        }
    }
    render(props){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#ffffff'}}>
                <View style={{ backgroundColor: '#ffffff' }}>
                    <HeaderTabCommon title="Kalkulator" navigation={this.props.navigation} />
                </View>
                <ScrollView>
                <Text style={{marginTop:24, textAlign:'center'}}>Kurs saat ini</Text>
                    <View style={{flexDirection:'row', justifyContent:'center'}}>
                        <Text style={{fontWeight:'bold',textAlign:'center'}}> RP.</Text>
                        <Text style={{fontWeight:'bold',textAlign:'center',color:'#37cccc'}}>1 </Text>
                        <Text style={{fontWeight:'bold',textAlign:'center'}}> = RP.</Text>
                        <Text style={{fontWeight:'bold',textAlign:'center',color:'#52bd44'}}> 3.392,8</Text>
                    </View>
                    <View style={{margin:16}}>
                        <View style={{...styles.box2, ...{borderTopLeftRadius:3,borderTopRightRadius:4}}}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16, marginLeft: 20, marginRight: 12, marginVertical: 17, color: '#333333' }}>RM</Text>
                                <TextInput placeholder="0" style={{ fontSize: 18, width:wp('37%')}} keyboardType='numeric'/>
                            </View>
                            <View style={{ width: 90, backgroundColor: '#153e74' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../../../assets/images/malayflag.png')} style={styles.inBox1} />
                                    <Text style={styles.txt2}>RM</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{...styles.box2,...{borderBottomLeftRadius:4,borderBottomRightRadius:4}}}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16, marginLeft: 20, marginRight: 14, marginVertical: 17, color: '#333333' }}>RP</Text>
                                <TextInput placeholder="0" style={{ fontSize: 18, width:wp('37%')}} keyboardType='numeric' />
                            </View>
                            <View style={{ width: 90, backgroundColor: '#153e74' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../../../assets/images/indoflag.png')} style={styles.inBox1} />
                                    <Text style={styles.txt2}>RP</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={{ marginTop: 40, height: 2, backgroundColor: '#e0e0e0' }} />
                    <Image source={require('../../../../assets/images/kalkulator.png')} style={{resizeMode:'contain',height:255,width:280, margin:40}}/>
                    </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    box2:{
        width:'100%',
        borderRadius:1,
        borderColor:'#828282',
        // backgroundColor:'#e0e0e0',
        height:60,
        // marginTop:8,
        borderWidth:1,
        flexDirection:'row',
        // paddingVertical:7,
        // paddingLeft:14,
        // marginTop:8,
        justifyContent:'space-between',
    },
    txt2:{marginTop:20, color:'white', marginLeft:6},
    inBox1:{width:32, height:32,marginVertical:14,marginLeft:14, marginRight:8},
})
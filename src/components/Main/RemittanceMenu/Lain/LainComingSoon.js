import React, { Component } from 'react';
import { Image, Text,SafeAreaView, TouchableOpacity, View,StyleSheet,Modal } from 'react-native';

export default class LainComingSoon extends Component{
    render(props){
        return(
            <SafeAreaView style={{flex:1}}>
                <View style={{flex:1, justifyContent:'center', alignItems:'center', alignContent:'center',paddingHorizontal:16}}>
                    <Image source={require('../../../../assets/images/comingsoon.png')} style={{resizeMode:'contain',width:288, height:255}} />
                    <Text style={{textAlign:'center',marginTop:40,fontSize:20, fontWeight:'bold'}}>Coming soon !</Text>
                    <Text style={{textAlign:'center',fontSize:12}}>Layanan ini masih dalam pengembangan, Anda dapat melakukan transaksi lain.</Text>
                </View>
                <View style={{height:80,padding:18}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Lain')}>
                        <View style={{ width: '100%', justifyContent: 'center', height: 45, borderWidth: 1, borderColor: '#d91c16', borderRadius: 4, paddingHorizontal: 16, backgroundColor: '' }}>
                            <Text style={{ color: '#D91C16', fontSize: 16, textAlign: 'center', }}>Kembali, Lakukan Transaksi lain </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}
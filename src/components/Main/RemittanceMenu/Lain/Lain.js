import React, { Component } from 'react';
import { Image, Text, TouchableOpacity, View,StyleSheet,Modal, SafeAreaView } from 'react-native';
import {HeaderTabCommon} from '../../../../containers/atom'

export default class Lain extends Component{
    render(props){
        return(
            <SafeAreaView style={{flex:1}}>
                <View style={{flex:1}}>
                    <HeaderTabCommon title="Lain-lain" navigation={this.props.navigation} />
                    
                    <View style={{marginTop:14, marginHorizontal:16}}>
                        <Text style={{fontSize:13,color:'#153e74'}}>Berikut adalah layanan chanelling dengan Bank di Indonesia yang dapat digunakan</Text>
                        <View style={{marginTop:40}}> 
                            <TouchableOpacity onPress={()=>this.props.navigation.navigate('LainComingSoon')}>
                                <View style={styles.bx1}>
                                    <View style={styles.bx2} />
                                    <Text style={{ fontSize:13, marginTop: '3%' }}>Pembukaan Tabungan</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                            <View style={{...styles.bx1,...{backgroundColor:'#FCF4DB'}}}>
                                <View style={{...styles.bx2,...{backgroundColor:"#F2C94C"}}}/>
                                <Text style={{fontSize:13,marginTop:'3%'}}>Pembukaan Deposito Berjangka</Text>
                            </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                            <View style={{...styles.bx1,...{backgroundColor:'#D5E6FB'}}}>
                                <View style={{...styles.bx2,...{backgroundColor:"#2F80ED"}}}/>
                                <Text style={{fontSize:13,marginTop:'3%'}}>Reksadana</Text>
                            </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                            <View style={{...styles.bx1,...{backgroundColor:'#EBDCF9'}}}>
                                <View style={{...styles.bx2,...{backgroundColor:'#9B51E0'}}}/>
                                <Text style={{fontSize:13,marginTop:'3%'}}>Consumer Loan</Text>
                            </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    bx1:{
        width:'100%',
        height:60,
        backgroundColor:'#D3EADD',
        borderRadius:3,
        flexDirection:'row',
        paddingLeft:16,
        paddingTop:10,
        marginBottom:14,
    },
    bx2:{
        width:40,
        height:40,
        backgroundColor:'#219653',
        borderRadius:6,
        marginRight:14,
    }
})


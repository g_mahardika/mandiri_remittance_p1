import React, { Component } from 'react';
import { Image,SafeAreaView, StyleSheet,Text, TextInput, TouchableOpacity, View, Modal, ScrollView } from 'react-native';
import { HeaderTabCommon, BottomBlue } from '../../../../containers/atom';
import { BottomKonfirmasi } from '../../../../containers/molecules';
import {PembayaranTagihanKonfirmasi} from '../../../../containers/organisms';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';



export default class TagihanPDAMKonfirmasi extends Component{
    render(props){
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor:'#ffffff', }}>
                <HeaderTabCommon title="Konfirmasi Transaksi" navigation={this.props.navigation} />
                <ScrollView >
                    <View style={{ flex: 1, marginBottom:hp('26%') }}>
                        <PembayaranTagihanKonfirmasi
                            txt1='Detail Transaksi'
                            txt2='Jenis Pembayaran'
                            txt3='Tagihan PDAM'
                            txt4='ID Pelanggan'
                            txt5='111222333444555'
                            txt6='Mayuga Wicaksana'
                            txt7='Periode'
                            txt8='Maret 2020'
                            txt9='Jumlah tagihan'
                            text1='Rp. 320.000 = RM 94.2458'
                            text2='Denda'
                            text3='RM 0'
                            text4='Biaya Admin'
                            text5='RM 1'
                            text6='Total Bayar'
                            text7='RM 95.2458'
                        />
                    </View>
                    <View style={{height:100}}>
                        <BottomKonfirmasi navigation={this.props.navigation} navigasi='TagihanPDAMPIN' />

                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}


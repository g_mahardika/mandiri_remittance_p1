import React, { Component } from 'react';
import {View,Text,StyleSheet, Image, TextInput, TouchableOpacity,Modal, ScrollView, ImageBackground,SafeAreaView } from 'react-native';
import { widthPercentageToDP as wp,heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { HeadSukses } from '../../../../containers/molecules';

export default class TagihanBPJSSukses extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false,
        }
    }
    render(props){
        return(
            <ScrollView style={{backgroundColor:'white'}} >
                <SafeAreaView style={{flex:1, backgroundColor:'#ffffff' }}>
                    <View style={{ flex: 1}}>
                        
                        <HeadSukses />

                        <View style={{flexDirection:'row', justifyContent:'space-between', marginRight:"27%"}}>
                        <View style={{ marginTop: 40, marginLeft: 16 }}>
                            <Text style={{ fontSize: 12, color: '#828282' }}>Jenis Pembayaran</Text>
                            <Text style={{ marginTop: 8 }}>Tagihan BPJS</Text>
                        </View>
                        <View style={{ marginTop: 40, marginLeft: 16 }}>
                            <Text style={{ fontSize: 12, color: '#828282' }}>Periode</Text>
                            <Text style={{ marginTop: 8 }}>Maret 2020</Text>
                        </View>
                        </View>
                        <View style={{ marginTop: 20, marginLeft: 16 }}>
                            <Text style={{ fontSize: 12, color: '#828282' }}>ID Pelanggan</Text>
                            <Text style={{ marginTop: 8, }}>111222333444555</Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Mayuga Wicaksana </Text>
                        </View>

                        <View style={{ marginTop: 20, marginLeft: 16, flexDirection: 'row' }}>

                            <Text style={{ fontSize: 12, color: '#828282' }}>Total Tagihan</Text>

                            <View style={{ flexDirection: 'row', marginTop: 27, marginLeft: '-21%', justifyContent: 'space-between' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 14, marginRight: 4 }}>RM.</Text>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>95.2548</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{ height: 60, paddingHorizontal: 16, marginTop:hp('25%') }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('MainHome')}
                        >
                            <View style={{ width: '100%', height: 45, borderColor: '#27ae60', borderWidth: 1, borderRadius: 4, justifyContent: 'center', alignItems: 'center' }} >
                                <Text style={{ fontSize: 14 }}>Kembali ke Halaman Utama</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </ScrollView>
        )
    }
}
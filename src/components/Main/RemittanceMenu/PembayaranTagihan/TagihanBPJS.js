import React, { Component } from 'react';
import { Image,SafeAreaView, StyleSheet,Text, TextInput, TouchableOpacity, View, Modal } from 'react-native';
import { HeaderTabCommon, BottomBlue } from '../../../../containers/atom';
import { PembayaranTagihanAll } from '../../../../containers/organisms';

export default class TagihanBPJS extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false,
            pdam:'',
        }
    }
    render(props){
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor:'#ffffff' }}>
                <View style={{flex:1}}>
                    <HeaderTabCommon title="Tagihan BPJS" navigation={this.props.navigation} />
                    <PembayaranTagihanAll  
                        navigation={this.props.navigation}
                        img={require('../../../../assets/images/posterbpjs.png')}
                        txt1='No. VA Keluarga'
                        txt2='Masukkan No. VA Keluarga'
                        navigasi='TryBarcode'
                        img2={require('../../../../assets/icons/scanner.png')}
                        txt3='Periode Hingga'
                        txt4='Pilih Periode'
                        txt6='Bulan Maret 2020'
                        txt7='Bulan April 2020'
                        txt8="Bulan Mei 2020"
                        txt9='Bulan Juni 2020'
                        txt10='Pilih Area BPJS'
                        
                    />
                </View>
                <View style={{height:53,paddingHorizontal:16, backgroundColor:''}}>
                    <BottomBlue navigation={this.props.navigation} txt='Lihat Tagihan' txt2='TagihanBPJSKonfirmasi' />
                </View>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    modalContent:{
            backgroundColor:'#000000aa',
            padding:10,
            flex:1,
    },
})

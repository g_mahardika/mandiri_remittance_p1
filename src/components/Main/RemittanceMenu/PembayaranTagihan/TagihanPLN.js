import React, { Component } from 'react';
import { Image,SafeAreaView, StyleSheet,Text, TextInput, TouchableOpacity, View, Modal } from 'react-native';
import { HeaderTabCommon, BottomBlue } from '../../../../containers/atom';
import { PembayaranTagihanAll } from '../../../../containers/organisms';


export default class TagihanPLN extends Component{
    render(props){
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor:'#ffffff' }}>
                <View style={{flex:1}}>
                <HeaderTabCommon title="Tagihan PLN" navigation={this.props.navigation} />
                <PembayaranTagihanAll  
                    navigation={this.props.navigation}
                    img={require('../../../../assets/images/posterpulsa.png')} 
                    txt1='ID Pelanggan'
                    txt2='Masukkan ID Pelanggan'
                    navigasi='TryBarcode'
                    img2={require('../../../../assets/icons/scanner.png')}
                />
                </View>
                <View style={{height:50,paddingHorizontal:16, backgroundColor:''}}>
                    <BottomBlue navigation={this.props.navigation} txt='Lihat Tagihan' txt2='TagihanPLNKonfirmasi' />
                </View>
            </SafeAreaView>
        )
    }
}
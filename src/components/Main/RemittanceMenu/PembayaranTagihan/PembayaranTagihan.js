import React, { Component } from 'react';
import { Image,SafeAreaView, StyleSheet,Text, TextInput, TouchableOpacity, View, Modal } from 'react-native';
import {HeaderTabCommon} from '../../../../containers/atom';
import { TigaBayarTagihan } from '../../../../containers/molecules';

export default class PembayaranTagihan extends Component{
    render(props){
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor:'#ffffff' }}>
                <HeaderTabCommon title="Pembayaran Tagihan" navigation={this.props.navigation} />
                <View style={{flexDirection:'row',height:70, backgroundColor:'',marginLeft:16,marginTop:40}} >
                    <TigaBayarTagihan txt1='TagihanPLN' txt2='PLN' navigation={this.props.navigation} img={require('../../../../assets/images/plnicon.png')}  />
                    <TigaBayarTagihan txt1='TagihanPDAM' txt2='PDAM' navigation={this.props.navigation} img={require('../../../../assets/images/pdamicon.png')}  />
                    <TigaBayarTagihan txt1='TagihanBPJS' txt2='BPJS' navigation={this.props.navigation} img={require('../../../../assets/images/bpjsicon.png')}  />
                </View>
            </SafeAreaView>
        )
    }
}

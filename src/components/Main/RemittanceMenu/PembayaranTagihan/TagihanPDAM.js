import React, { Component } from 'react';
import { Image,SafeAreaView, StyleSheet,Text, TextInput, TouchableOpacity, View, Modal } from 'react-native';
import { HeaderTabCommon, BottomBlue } from '../../../../containers/atom';
import { PembayaranTagihanAll } from '../../../../containers/organisms';

export default class TagihanPDAM extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false,
            pdam:'',
        }
    }
    render(props){
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor:'#ffffff' }}>
                <View style={{flex:1}}>
                    <HeaderTabCommon title="Tagihan PDAM" navigation={this.props.navigation} />
                    <PembayaranTagihanAll  
                        navigation={this.props.navigation}
                        img={require('../../../../assets/images/posterpdam.png')}
                        txt1='ID Pelanggan'
                        txt2='Masukkan ID Pelanggan'
                        navigasi='TryBarcode'
                        img2={require('../../../../assets/icons/scanner.png')}
                        txt3='Area'
                        txt4='Pilih Area'
                        txt6='PDAM Kota Bandung'
                        txt7='PDAM Kabupaten Cianjur'
                        txt8="PDAM Kabupaten Bandung Barat"
                        txt9='PDAM Kota Bekasi'
                        txt10='Pilih Area PDAM'
                    />
                </View>
                <View style={{height:53,paddingHorizontal:16, backgroundColor:''}}>
                    <BottomBlue navigation={this.props.navigation} txt='Lihat Tagihan' txt2='TagihanPDAMKonfirmasi' />
                </View>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    modalContent:{
            backgroundColor:'#000000aa',
            padding:10,
            flex:1,
    },
})

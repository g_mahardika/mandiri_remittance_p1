import PembayaranTagihan from './PembayaranTagihan';
import TagihanPLN from './TagihanPLN';
import TagihanPLNKonfirmasi from './TagihanPLNKonfirmasi';
import TagihanPLNPIN from './TagihanPLNPIN'
import TagihanPLNSukses from './TagihanPLNSukses';
import TagihanPDAM from './TagihanPDAM';
import TagihanPDAMKonfirmasi from'./TagihanPDAMKonfirmasi';
import TagihanPDAMPIN from './TagihanPDAMPIN'
import TagihanPDAMSukses from './TagihanPDAMSukses';
import TagihanBPJS from './TagihanBPJS';
import TagihanBPJSKonfirmasi from './TagihanBPJSKonfirmasi';
import TagihanBPJSPIN from './TagihanBPJSPIN';
import TagihanBPJSSukses from './TagihanBPJSSukses';

export {
    PembayaranTagihan,
    TagihanPLN,
    TagihanPLNKonfirmasi,
    TagihanPLNPIN,
    TagihanPLNSukses,
    TagihanPDAM,
    TagihanPDAMKonfirmasi,
    TagihanPDAMPIN,
    TagihanPDAMSukses,
    TagihanBPJS,
    TagihanBPJSKonfirmasi,
    TagihanBPJSPIN,
    TagihanBPJSSukses,
}
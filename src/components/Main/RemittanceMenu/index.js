import KirimUang from './KirimUang/KirimUang';
import KirimUang2 from './KirimUang/KirimUang2';
import KirimUangKonfirmasi from './KirimUang/KirimUangKonfirmasi';
import KirimUangPIN from './KirimUang/KirimUangPIN';
import KirimUangSukses from './KirimUang/KirimUangSukses';
import Kalkulator from './KalkulatorRinggitRupiah/Kalkulator'
import Lain from './Lain/Lain';
import LainComingSoon from './Lain/LainComingSoon';
export {
    KirimUang,
    KirimUang2,
    KirimUangKonfirmasi,
    KirimUangPIN,
    KirimUangSukses,
    Kalkulator,
    Lain,
    LainComingSoon,
}
import React, { Component } from 'react';
import {View,Text,StyleSheet, Image, TextInput, TouchableOpacity,Modal, ScrollView, ImageBackground,SafeAreaView } from 'react-native';
import { widthPercentageToDP as wp,heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { HeadSukses } from '../../../../containers/molecules';

export default class KirimUangSukses extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false,
        }
    }
    render(props){
        return(
            <ScrollView style={{backgroundColor:'white'}} >
                <SafeAreaView style={{flex:1, backgroundColor:'#ffffff' }}>
                    <View style={{ flex: 1}}>
                        <HeadSukses />

                        <View style={{ marginTop: 40, marginLeft: 16 }}>
                            <Text style={{ fontSize: 12, color: '#828282' }}>Jenis Transaksi</Text>
                            <Text style={{ marginTop: 8 }}>Kirim Uang</Text>
                        </View>
                        <View style={{ marginTop: 20, marginLeft: 16 }}>
                            <Text style={{ fontSize: 12, color: '#828282' }}>Kirim ke</Text>
                            <Text style={{ marginTop: 8, fontSize: 14, fontWeight: 'bold' }}>Mayuga Wicaksana (Tabungan)</Text>
                            <Text>BNI - 081299325457</Text>
                        </View>

                        <Text style={{ marginLeft: 16, marginTop: 27, fontSize: 12, color: '#828282' }}>Nominal kirim</Text>
                        <View style={{ marginTop: 20, marginLeft: 16, flexDirection: 'row' }}>
                            <View style={{ flexDirection: 'row', marginTop: 0, marginLeft: 1, backgroundColor: '', justifyContent: 'space-between' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 14, marginRight: 4 }}>RM.</Text>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>147,895</Text>
                                </View>
                                <Image source={require('../../../../assets/icons/vector44.png')} style={{ width: 23, height: 17, marginHorizontal: 16, marginTop: 0 }} />
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 14, marginRight: 4 }}>RP.</Text>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>500.000</Text>
                                </View>
                            </View>
                        </View>
                    
                    </View>

                    <View style={{ height: 60, paddingHorizontal: 16, marginTop:hp('25%') }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('MainHome')}
                        >
                            <View style={{ width: '100%', height: 45, borderColor: '#27ae60', borderWidth: 1, borderRadius: 4, justifyContent: 'center', alignItems: 'center' }} >
                                <Text style={{ fontSize: 14 }}>Kembali ke Halaman Utama</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </ScrollView>
        )
    }
}

const styles=StyleSheet.create({
    modalContent:{
            backgroundColor:'#000000aa',
            padding:10,
            flex:1,
    },
})


import React, { Component } from 'react';
import {View,Text,StyleSheet,SafeAreaView, Image, TextInput, TouchableOpacity,Modal, ScrollView } from 'react-native';
import {HeaderTabCommon, BottomBlue,} from '../../../../containers/atom'
import {BottomKonfirmasi} from '../../../../containers/molecules'

export default class KirimUangKonfirmasi extends Component {
    render(props){
        return(
            <SafeAreaView style={{flex:1, backgroundColor:'#ffffff'}}>
                <HeaderTabCommon title="Konfirmasi Transaksi" navigation={this.props.navigation} />
                <ScrollView >
                    <View style={styles.box}>
                        <Text style={styles.txt2}>Detail Transaksi</Text>
                    </View>
                    <View style={{paddingHorizontal:16, marginTop:24, flex:1}}>
                        <Text style={{fontSize:13, color:'#828282'}}>Kirim uang ke:</Text>
                        <View style={styles.box1}>
                            <Image source={require('../../../../assets/images/ava.png')} style={{width:34, height:34, marginRight:13}}/>
                            <View>
                                <Text style={{fontSize:12}}>Mayuga Wicaksana (Tabungan)</Text>
                                <Text style={{fontSize:10, color:'#828282'}}>BNI - 08213719743</Text>
                            </View>
                        </View>
                        <Text style={{ fontSize: 13, color: '#828282', marginTop: 24 }}>Nominal kirim :</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 14 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text>Rp</Text>
                                <Text style={{ marginLeft: 5, fontWeight: 'bold', }}>147,895</Text>
                            </View>
                            <Image source={require('../../../../assets/icons/vector44.png')} />
                            <View style={{ flexDirection: 'row' }}>
                                <Text>Rp</Text>
                                <Text style={{ marginLeft: 5, fontWeight: 'bold' }}>500.000</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'space-between',marginTop:8}}>
                            <View style={{ width:'25%',borderWidth: 1, borderColor: '#e0e0e0', }} />
                            <View style={{ width:'25%',borderWidth:1, borderColor: '#e0e0e0', }} />
                        </View>
                        <Text style={{fontSize:13,color:'#828282',marginTop:24}}>Catatan :</Text>
                        <Text style={{marginTop:24,fontSize:13, color:'gray'}}>Notes</Text>
                    </View>
                </ScrollView>
                    
                    <BottomKonfirmasi navigation={this.props.navigation} navigasi='KirimUangPIN' />

            </SafeAreaView>
        )
    }
}


const styles=StyleSheet.create({
    txt:{
        fontSize:16,
        color:'#333333',
        marginTop:18,
    },
    box:{height:40,backgroundColor:'#e0e0e0',width:'100%',paddingVertical:12,paddingLeft:16,},
    txt2:{color:'#153e74', fontSize:13, fontWeight:'bold'},
    box1:{
        width:'100%',
        borderColor:'#27ae60',
        borderRadius:4,
        backgroundColor:'#e0e0e0',
        height:60,
        borderWidth:1,
        marginTop:8,
        flexDirection:'row',
        paddingVertical:13,
        paddingLeft:14,
    },
})
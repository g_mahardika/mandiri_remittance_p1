import React, { Component } from 'react';
import {SafeAreaView, View,Text,StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';
import { HeaderTabCommon, BottomWhite } from '../../../../containers/atom';
// import { widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import { FiveTransaction } from '../../../../containers/molecules';

export default class KirimUang extends Component {
    render(props){
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor:'#ffffff' }}>
                <HeaderTabCommon title="Kirim Uang" navigation={this.props.navigation} />
                <View style={{ flex: 1,paddingHorizontal:16 }}>
                    {/* <View style={{paddingLeft:16, paddingRight:16}}> */}
                    <View style={{ width: '100%', height: 40, backgroundColor: '#e0e0e0', borderRadius: 4, flexDirection: 'row', }}>
                        <Image source={require('../../../../assets/icons/searchImg.png')} style={{ width: 20, height: 20, margin: 10 }} />
                        <TextInput placeholder="Cari Penerima" />
                    </View>
                    <Text style={{ fontSize: 13, alignItems: 'center', marginTop: 21, fontWeight: 'bold' }}>Kontak Penerima</Text>
                    <View style={{ marginTop: 24 }}>
                        <FiveTransaction navigation={this.props.navigation} 
                            img={require('../../../../assets/images/ava.png')} 
                            txt1="Mayuga Wicaksana (Tabungan)" 
                            txt2="BNI - 08213719743" 
                            txt6='KirimUang2'  />
                        <FiveTransaction navigation={this.props.navigation} 
                            img={require('../../../../assets/images/ava2.png')} 
                            txt1="Lily Steward (Orangtua) " 
                            txt2="Mandiri - 08213719743" 
                            txt6='KirimUang'  />
                        <FiveTransaction navigation={this.props.navigation} 
                            img={require('../../../../assets/images/ava3.png')} 
                            txt1="Priscilla Bell (Anak)" 
                            txt2="Mandiri Syariah - 08213719743"  
                            txt6='KirimUang'  />
                        <FiveTransaction navigation={this.props.navigation} 
                            img={require('../../../../assets/images/ava4.png')} 
                            txt1="Jenny Henry (Pasangan)" 
                            txt2="BNI - 08213719743" 
                            txt6='KirimUang'  />
                    </View>
                    {/* </View> */}
                </View>
                <View style={{ marginTop:100,paddingHorizontal:16 }}>
                    <BottomWhite navigation={this.props.navigation} txt='+ Tambah Penerima Baru ' txt2='KirimUang' style={{ borderColor: '#153e74' }} />
                </View>
            </SafeAreaView>
        )
    }
}
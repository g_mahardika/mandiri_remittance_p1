import React, { Component } from 'react';
import {View,Text,StyleSheet,SafeAreaView, Image, TextInput, TouchableOpacity,Modal, ScrollView } from 'react-native';
import {HeaderTabCommon, BottomBlue,} from '../../../../containers/atom'
import { widthPercentageToDP as wp, } from 'react-native-responsive-screen';

export default class KirimUang2 extends Component {
    render(props){
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor:'#ffffff' }}>
                <HeaderTabCommon title="Kirim Uang" navigation={this.props.navigation} />
                <ScrollView style={{flex:1}}>
                    <Text style={{ marginTop: 24, marginLeft: 16, fontSize: 13, }}>Kirim uang ke :</Text>
                    <View style={{ paddingHorizontal: 16, }}>
                        <View style={styles.box1}>
                            <Image source={require('../../../../assets/images/ava.png')} style={{ width: 34, height: 34, marginRight: 13 }} />
                            <View>
                                <Text style={{ fontSize: 12 }}>Mayuga Wicaksana (Tabungan)</Text>
                                <Text style={{ fontSize: 10, color: '#828282' }}>BNI - 08213719743</Text>
                            </View>
                        </View>
                        <Text style={{ marginTop: 20, marginBottom: 8,fontSize:13 }}>Nominal:</Text>
                        <View style={styles.box2}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16, marginLeft: 20, marginRight: 12, marginVertical: 17, color: '#333333' }}>RM</Text>
                                <TextInput placeholder="0" keyboardType='numeric' style={{ width:wp('41%'), fontSize: 18, marginLeft: 3 }} />
                            </View>
                            <View style={{ width: 90, backgroundColor: '#153e74' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../../../assets/images/malayflag.png')} style={styles.inBox1} />
                                    <Text style={styles.txt2}>RM</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.box2}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 16, marginLeft: 20, marginRight: 14, marginVertical: 17, color: '#333333' }}>RP</Text>
                                <TextInput placeholder="0" keyboardType='numeric' style={{width:wp('41%'), fontSize: 18, marginLeft: 3 }} />
                            </View>
                            <View style={{ width: 90, backgroundColor: '#153e74' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../../../../assets/images/indoflag.png')} style={styles.inBox1} />
                                    <Text style={styles.txt2}>RP</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                            <Text style={{ fontSize: 13 }}>Catatan</Text>
                            <Text style={{ color: '#153e74', marginLeft: 3, marginTop:0, fontSize:13 }}>(opsional)</Text>
                        </View>
                        <View style={styles.box3}>
                            <TextInput placeholder="Tambahkan catatan" />
                        </View>

                    </View>
                </ScrollView>
                <View style={{marginHorizontal:16} }>
                    <BottomBlue txt='Kirim Sekarang' navigation={this.props.navigation} txt2='KirimUangKonfirmasi' />

                </View>
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    txt:{
        fontSize:16,
        color:'#333333',
        marginTop:18,
    },
    box1:{
        width:'100%',
        borderColor:'#27ae60',
        borderRadius:4,
        backgroundColor:'#e0e0e0',
        height:60,
        borderWidth:1,
        marginTop:8,
        flexDirection:'row',
        paddingVertical:13,
        paddingLeft:14,
    },
    box2:{
        width:'100%',
        borderRadius:1,
        borderColor:'#828282',
        // backgroundColor:'#e0e0e0',
        height:60,
        // marginTop:8,
        borderWidth:1,
        flexDirection:'row',
        // paddingVertical:7,
        // paddingLeft:14,
        // marginTop:8,
        justifyContent:'space-between',
    },
    box3:{
        width:'100%',
        borderColor:'#828282',
        borderRadius:2,
        // backgroundColor:'#e0e0e0',
        height:56,
        borderWidth:1,
        marginTop:8,
        flexDirection:'row',
        paddingVertical:8,
        paddingLeft:14,
    },
    inBox1:{width:32, height:32,marginVertical:14,marginLeft:14, marginRight:8},
    txt2:{marginVertical:16,marginLeft:1,fontSize:16,color:'#ffffff', }

})
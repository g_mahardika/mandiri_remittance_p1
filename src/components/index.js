import Opening from './Opening';
import Greeting from './Greeting';
import AccountAndRegistration from "./Login/AccountAndRegistration";
import AccountAndRegistration2 from "./Login/AccountAndRegistration2";
import AccountAndRegistration3 from "./Login/AccountAndRegistration3";
import AccountAndRegistration4 from "./Login/AccountAndRegistration4";
import AccountAndRegistration5 from "./Login/AccountAndRegistration5";
import AccountAndRegistration6 from "./Login/AccountAndRegistration6";
import LoginForm from './Login/LoginForm';
import MainHome from './Main/MainHome';
import RiwayatTransaksi from './Transaction/RiwayatTransaksi';
import Akun from './Account/Account'
import AkunUbahSandi from './Account/AkunUbahSandi';
import AkunUbahPIN from './Account/AkunUbahPIN';
import AkunPINBaru from './Account/AkunPINBaru';
import AkunKonfirmasiPIN from './Account/AkunKonfirmasiPIN';
import TentangAplikasi from './Account/TentangAplikasi';
import HubungiKami from './Account/HubungiKami';

export {
    Opening,
    Greeting,
    AccountAndRegistration,
    AccountAndRegistration2,
    AccountAndRegistration3,
    AccountAndRegistration4,
    AccountAndRegistration5,
    AccountAndRegistration6,
    LoginForm,
    MainHome,
    RiwayatTransaksi,
    Akun,
    AkunUbahSandi,
    AkunUbahPIN,
    AkunPINBaru,
    AkunKonfirmasiPIN,
    TentangAplikasi,
    HubungiKami,
}
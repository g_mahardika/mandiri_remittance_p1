import React, { Component} from 'react';
import { Image, Modal, StyleSheet, Text, TouchableOpacity, View, SafeAreaView } from 'react-native';
import {FiveTransaction} from '../../containers/molecules';
import {FilterTransaksi} from '../../containers/organisms';


import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export default class RiwayatTransaksi extends Component{
    constructor(props){
        super(props)
        this.state={
            modalOpen:false,
        }
    }
    render(props){
        return (
            <SafeAreaView>
                {/* <HeaderTab title="Riwayat Transaksi"/> */}
                <Text style={{marginLeft:16, fontSize:16, marginVertical:10,textAlign:'center'}}>Riwayat Transaksi</Text>
                <View style={styles.head2}>
                    <Text style={{ fontWeight: 'bold', }}>5 Transaksi terakhir</Text>
                    <TouchableOpacity onPress={()=>this.setState({modalOpen:true})}>
                        <View style={styles.box}>
                            <Text style={{ fontSize: 11, marginRight: 11, }}>Filter Transaksi</Text>
                            <Image source={require('../../assets/icons/arrowblue.png')} style={styles.img} />
                        </View>
                    </TouchableOpacity>

                    <Modal visible={this.state.modalOpen} transparent={this.state.modalOpen} animationType="slide" >

                        <View style={styles.modalContent}>
                            
                            <View style={{ backgroundColor: 'white', flex: 1, marginTop:hp ('15%'), borderRadius: 10,marginBottom: hp('20%') }}>
                                <FilterTransaksi navigation={this.props.navigation}  />
                                <View style={{marginHorizontal:16, marginTop:16}}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ modalOpen: !this.state.modalOpen })}
                                        style={styles.box2}
                                    >
                                        <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#153e74', textAlign: 'center' }}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>


                </View>
                <View style={{ width: '100%', borderWidth: 1, borderColor: '#e0e0e0', marginTop: hp('0.1%'), }} />
                <View style={{ marginVertical: hp('2%'),marginHorizontal:16 }}>
                    <FiveTransaction navigation={this.props.navigation}
                        txt1="Top Up" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="+ RM." txt5="30,21"
                        img={require('../../assets/icons/in.png')} style={{ color: '#27ae60' }}

                    />
                    <FiveTransaction navigation={this.props.navigation}
                        txt1="Kirim Uang - Orang Tua" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="- RM." txt5="30,21"
                        img={require('../../assets/icons/out.png')} style={{ color: '#EB5757' }}
                    />
                    <FiveTransaction navigation={this.props.navigation}
                        txt1="Top Up" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="+ RM." txt5="30,21"
                        img={require('../../assets/icons/in.png')} style={{ color: '#27ae60' }}
                    />
                    <FiveTransaction navigation={this.props.navigation}
                        txt1="Kirim Uang - Orang Tua" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="- RM." txt5="30,21"
                        img={require('../../assets/icons/out.png')} style={{ color: '#EB5757' }}
                    />
                    <FiveTransaction navigation={this.props.navigation}
                        txt1="Top Up" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="+ RM." txt5="30,21"
                        img={require('../../assets/icons/in.png')} style={{ color: '#27ae60' }}
                    />
                </View >
            </SafeAreaView>
        )
    }
}

const styles=StyleSheet.create({
    body:{
        flex:1,
    },
    head2:{
        height:50,
        flexDirection:'row',
        justifyContent:'space-between',
        paddingHorizontal:16,
    },
    box:{
        borderWidth:1,
        borderColor:'#153e74',
        borderRadius:24,
        paddingVertical:6,
        paddingLeft:14,
        flexDirection:'row',
        height:27,
        width:120,
    }, 
    box2:{
        width:'100%',
        borderRadius:3,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10,
        backgroundColor:'#ffffff',
        borderColor:'#153e74',
        borderWidth:0.8,
        
    },
    img:{
        width:8,
        height:5,
        marginTop:4,
    },
    img2:{
        width:20,
        height:20,
    },
    headArrow:{
        height:56,
        width:'100%',
        paddingLeft:16,
        flexDirection:'row',
        paddingTop:18,
    },
    modalContent:{
        backgroundColor:'#000000aa',
        padding:10,
        flex:1,
        // height:hp()
    }
})
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation'

import OpeningScreen from '../screens/Opening';
import ChoseScreen from '../screens/ChoseScreen';

const Navigation = createStackNavigator({
    Opening:{
        screen:OpeningScreen,
        navigationOptions:{
            headerShown:false,
        }
    },
    ChoseScreen:{
        screen:ChoseScreen,
        navigationOptions:{
            headerShown:false,
        }
    },
},{

})
const MainNavigation = createAppContainer(Navigation);
export default MainNavigation;
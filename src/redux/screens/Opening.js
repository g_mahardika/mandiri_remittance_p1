import React, { Component } from 'react';
import { View,StyleSheet, Image, TouchableOpacity } from 'react-native';

export default class OpeningScreen extends Component {
    render(){
        return(
            <TouchableOpacity 
                onPress={()=> this.props.navigation.navigate('ChoseScreen')}
            style={styles.container}>
                <Image source={require('../assets/images/mandiriwhite.png')}  style={styles.img}/>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#153e74',
        justifyContent:'center',
        alignItems:'center'
    },
    img:{
        resizeMode:'contain',
        width:100,
        height:150,
    }
})
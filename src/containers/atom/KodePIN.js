import React,{Component} from 'react';
import { Image,SafeAreaView ,ScrollView, StyleSheet,Alert, Text, TextInput, TouchableOpacity, View } from 'react-native';
import CodePin from 'react-native-pin-code'

export default class KodePIN extends Component{
    render(){
        return(
            <View style={{ marginTop: 30, width: '100%'}}>
                
                <CodePin 
                    number={6}
                    code = "123456"
                    checkPinCode={(code, callback) => callback(code === '123456')}
                    success={()=> this.props.navigation.navigate(`${this.props.txt}`)}
                    text = ""
                    keyboardType='numeric'

                    error = {()=> alert("Your password is wrong")}
                    // autoFocusFirst={false}
                    obfuscation={true}
                    containerStyle={{
                        height:100,
                        marginTop:-30
                    }}
                    containerPinStyle={{
                        marginTop:0,
                        flexDirection:'row',
                        // justifyContent:'space-around',
                    }}
                    pinStyle={{
                        
                        textAlign:'center',
                        backgroundColor:'#e8e9eb',
                        marginLeft:3,
                        borderRadius:3,
                        width:30,
                        height:40,
                        shadowColor:'#000000',
                        shadowOffset:{
                            shadowRadius:3,
                            shadowOpacity:0.4
                        },
                        
                    }}
                    textStyle={{
                        textAlign: 'center', 
                        color: 'gray', 
                        fontSize: 20, 
                        marginTop: 7,
                       
                    }}
                />
            </View>
        )
    }
}

const styles=StyleSheet.create({
    sixBox:{
        width:40, 
        marginRight:14,
        height:40, 
        borderColor:'#27ae60',
        borderWidth:1,
        borderRadius:2,
        justifyContent:'center', 
        alignContent:'center', 
        alignItems:'center',
        flex:1
    }
})



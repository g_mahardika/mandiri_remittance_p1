import React, { Component,useState } from 'react';
import {View,Text,TouchableOpacity, StyleSheet} from 'react-native';

export default class ButtonNextFooter extends Component{
    
    render(props){
        return(
            <View>
                <View style={{ width: '100%', height: 68, paddingHorizontal: 16,backgroundColor:'#ffffff' }}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate(`${this.props.txt}`) }
                    >
                        <View style={styles.touch}>
                            <Text style={{ color: 'white', fontSize: 16, }}>Selanjutnya</Text>
                        </View>

                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles=StyleSheet.create({
    touch:{
        width:'100%',
        borderWidth:1,
        borderColor:'#BDBDBD',
        borderRadius:4,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        marginTop:14,
        paddingVertical:12,
        backgroundColor:"#153e74"
    },
})
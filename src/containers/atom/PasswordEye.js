import React, {Component} from 'react';
import { Image,SafeAreaView ,ScrollView, StyleSheet, Text, TouchableOpacity,TextInput, View } from 'react-native';

export default class PasswordEye extends Component{
    constructor(){
        super()
        this.state={
            security:true,
            password:'',
        }
    }
    render(props){
        return(
            <View>
                <Text style={{ marginTop: 20, color: '#333333' }}>{this.props.text1}</Text>
                <View style={{ flexDirection: 'row', width: '100%', borderColor: 'black', borderWidth: 1, height: 40, marginTop: 8, justifyContent: 'space-between', paddingHorizontal: 14, paddingVertical: 0 }}>
                    <TextInput {...this.props} placeholder={`${this.props.txt2}`} secureTextEntry={this.state.security} style={{ flex: 1 }} />
                    <TouchableOpacity onPress={() => this.setState({ security: !this.state.security })}>
                        <Image style={{ marginTop: 12, width: 24, height: 12 }} source={require('../../assets/icons/eye.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
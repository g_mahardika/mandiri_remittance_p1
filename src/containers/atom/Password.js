import React, {Component} from 'react';
import { Image,SafeAreaView ,ScrollView, StyleSheet, Text, TouchableOpacity,TextInput, View } from 'react-native';

export default class Password extends Component{
    render(props){
        return(
            <View>
                <Text style={{ marginTop: 20, color: '#333333', marginBottom:8 }}>{this.props.text1}</Text>
                
                <TextInput {...this.props} placeholder={`${this.props.text2}`} secureTextEntry={true} style={{height:40, borderWidth:1, width:'100%',borderRadius:3 ,borderWidth:1,paddingLeft:14}} />
                    
            </View>
        )
    }
}
import React, { Component } from 'react';
import {View,TextInput, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export default class TextAndInput extends Component{
    render(props){
        const {placeholder} = this.props;
        return(
            <View >
                <Text style={styles.txt }>{this.props.txt}</Text>
                <TextInput placeholder={`${placeholder}`} style={styles.input} />
            </View>
        )
    }
}

const styles=StyleSheet.create({
    txt:{
        fontSize: 12, 
        marginBottom: 8, 
        marginTop: 24 
    },
    input:{
        width: '100%', 
        height:40,
        borderRadius:3 ,
        borderWidth:1,
        paddingLeft:14,
        color:'grey',
    }
})
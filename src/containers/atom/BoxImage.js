import React, { Component } from 'react';
import {TouchableOpacity, StyleSheet,Image,Text, TextInput,View} from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';


 
const PendingView=()=>(
    <View
        style={{flex:1, backgroundColor:'green', justifyContent:'center', alignItems:'center'}}
    >
        <Text>Waiting</Text>
    </View>
)
class BoxImage extends Component{
    render(props){
        return (
            <View style={styles.box}>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('TryCamera2')}>
                    <Image source={require('../../assets/images/camera1.png')} style={{ resizeMode: 'contain', width: 60, height: 50 }} />
                </TouchableOpacity>
                <Text style={{ marginTop: 14, fontSize: 12, color: '#153e74' }}>Ambil Foto</Text>
                
            </View>
        )
    }
}
export default BoxImage;

const styles=StyleSheet.create({
    box:{
        borderRadius:4,
        width:'100%',
        // height:180,
        height:hp('21%'), 
        borderColor:'black',
        borderWidth:1,
        alignItems:'center',
        justifyContent:'center',
    },
    img:{
        marginTop:54,
        width:'0.2%',
        height:40,
        marginHorizontal:134,
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },

})
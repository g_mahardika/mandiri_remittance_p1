import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView} from 'react-native';

export default class BottomWhite extends Component{
    render(){
        return(
            <TouchableOpacity onPress={()=> this.props.navigation.navigate(`${this.props.txt2}`)}>
                <View style={{...styles.box , ...this.props.style}}>
                    <Text style={styles.txt}>{this.props.txt}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles=StyleSheet.create({
    box:{
        width:'100%',
        borderRadius:3,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10,
        backgroundColor:'#ffffff',
        borderColor:'black',
        borderWidth:1,
    },
    txt:{
        fontSize:14,
        color:'#153e74',
    }
})
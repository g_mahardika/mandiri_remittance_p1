import BottomBlue from './BottomBlue';
import BottomWhite from './BottomWhite';
import HeaderTab from './HeaderTab';
import TextAndInput from './TextAndInput';
import BoxImage from './BoxImage';
import InlineWords from './InlineWords';
import ButtonNextFooter from './ButtonNextFooter';
import HeaderTabCommon from './HeaderTabCommon';
import Password from './Password';
import PasswordEye from './PasswordEye';
import KodePIN from './KodePIN';


export {
    BottomBlue,
    BottomWhite,
    HeaderTab,
    TextAndInput,
    BoxImage,
    InlineWords,
    ButtonNextFooter,
    HeaderTabCommon,
    Password,
    PasswordEye,
    KodePIN,

}
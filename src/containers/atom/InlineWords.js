import React, { Component } from 'react';
import {TouchableOpacity, StyleSheet,Image,Text, TextInput,View} from 'react-native';


const InLineWords=(props)=>{
    return(
        <View style={{flexDirection:'row',marginTop:23, justifyContent:'space-between', marginBottom:8}}>
            <Text style={{fontSize:12}}>{props.txt1}</Text>
            <Text style={{ color: '#153E74',fontSize:12 }}>{props.txt2}</Text>
        </View>
    )
}
export default InLineWords;


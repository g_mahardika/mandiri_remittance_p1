import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView} from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export default class BottomBlue extends Component{
    render(props){
        return(
            <TouchableOpacity onPress={()=> this.props.navigation.navigate(`${this.props.txt2}`)}>
                <View style={styles.box}>
                    <Text style={styles.txt}>{this.props.txt}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles=StyleSheet.create({
    box:{
        width:'100%',
        borderRadius:3,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10,
        backgroundColor:'#153e74',
    },
    txt:{
        fontSize:14,
        color:'#ffffff',
    }
})
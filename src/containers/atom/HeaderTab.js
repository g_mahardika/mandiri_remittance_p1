import React, { Component } from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

export default class HeaderTab extends Component{
    render(props){
        return(
            <View>
                <View style={{ height: 56, backgroundColor: '#ffffff' }}>
                    <View style={styles.headerTitleBox}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate(`${this.props.txt}`)}
                        >
                            <Image
                                source={`${this.props.icn}`}
                                style={styles.arrow}
                            />
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}> {this.props.title}</Text>
                    </View>
                </View>
            </View>
        )   
    }
}

const styles= StyleSheet.create({
    headerTitleBox:{
        width:'100%',
        flexDirection:'row',
        paddingLeft:18,
        paddingVertical:18
    },
    arrow:{
        width:21,
        height:18,
        top:0,
        right:8,
    },
    headerTitle:{
        fontSize:16,
        lineHeight:20,
        width:268,
    },
})
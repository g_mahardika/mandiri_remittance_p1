import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';


class RemittanceMenuBasic extends Component{
    render(props){
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate(`${this.props.txt2}`)} >
                <View style={{ alignItems: 'center', marginLeft: 16 }}>
                    <View style={{ ...styles.box, ...this.props.style }}>
                    {/* <View style={styles.box}> */}
                        <View style={{ position: 'absolute', }}>
                            <Image source={this.props.img} style={styles.img} />
                            <Text style={styles.txt}>{this.props.txt}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

export default RemittanceMenuBasic;

const styles = StyleSheet.create({
    box:{
        width: wp('43%'),
        flex:1, 
        height: 100,
        borderWidth: 1,
        borderRadius: 8,
    },
    img:{
        marginLeft:14,
        marginTop:9,
        width:wp('10%'),
        height:40,
        resizeMode:'contain',
    },
    txt:{
        marginLeft:14,
        fontSize:12,
        marginTop:5,
        color:'white',
    },
    
})





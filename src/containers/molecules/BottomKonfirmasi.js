import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView} from 'react-native';

export default class BottomKonfirmasi extends Component{
    render(props){
        return(
            <View>
                <View style={{paddingHorizontal:16, backgroundColor:''}}>
                    <Text style={{ textAlign: 'center' }}>Apakah Anda yakin akan melanjutkan proses transaksi ini?</Text>
                    <View style={{ marginTop: 16, justifyContent: 'space-between', marginBottom: 4, flexDirection: 'row', }}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("MainHome")}
                        >
                            <Text style={{ fontSize: 16, color: '#eb5757', marginTop: 10, marginLeft: 32 }}>Batal</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate(`${this.props.navigasi}`)}
                        >
                            <View style={{ width: 157, height: 46, borderRadius: 4, backgroundColor: '#153e74', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 15, color: '#ffffff' }}>Ya</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
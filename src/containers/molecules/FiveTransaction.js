import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';



class FiveTransaction extends Component{
    render(props){
        return (
            <TouchableOpacity onPress={()=> this.props.navigation.navigate(`${this.props.txt6}`)}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <View style={{ flexDirection: 'row', flex:1 }}>

                        <Image source={this.props.img} style={styles.img} />

                        <View >
                            <Text style={styles.txt}>{this.props.txt1}</Text>
                            <Text style={styles.txt2}>{this.props.txt2}</Text>
                        </View>

                    </View>

                    <View style={{width:wp('31%')}}>

                        <Text style={styles.txt2}>{this.props.txt3}</Text>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.txt3}>{this.props.txt4}</Text>
                            <Text style={{ ...styles.txt4, ...this.props.style }}>{this.props.txt5}</Text>
                        </View>

                    </View>
                </View>
                <View style={styles.line} />
            </TouchableOpacity>
        )
    }
}
export default FiveTransaction;

const styles = StyleSheet.create({
    img:{
        width:34,
        height:34,
        marginRight:13,
    },
    txt :{
        fontSize:12,
        color:'#333333',
    },
    txt2:{
        fontSize:12,
        color:'#828282',
    },
    txt3:{
        color:'#27ae60',
        fontSize:12, 
    },
    txt4:{
        fontWeight:'bold',
        color:'#27ae60',
        // color:(`${this.props.color}`),
        fontSize:12, 
    },
    line:{
        borderColor:'#e0e0e0', 
        borderWidth:1,
        marginLeft:45,
        marginRight:16, 
        width:'87%',
        marginVertical:14
    }

})
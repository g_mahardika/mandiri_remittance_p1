import React, { Component } from 'react';
import { Image,SafeAreaView, StyleSheet,Text, TextInput, TouchableOpacity, View, Modal } from 'react-native';


export default class TigaBayarTagihan extends Component{
    render(props){
        return(
            <TouchableOpacity onPress={()=> this.props.navigation.navigate(`${this.props.txt1}`)}>
                <View style={{ flexDirection: 'column', height: 70, width: 63, marginRight: 16 }}>
                    <Image source={this.props.img} style={{ resizeMode: 'contain', width: 40, height: 40 }} />
                    <Text style={{ marginTop: 8, marginLeft: 7, fontSize: 13 }}>{this.props.txt2}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}
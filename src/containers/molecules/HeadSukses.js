import React, { Component } from 'react';
import {View,Text,StyleSheet, Image, TextInput, TouchableOpacity,Modal, ScrollView, ImageBackground,SafeAreaView } from 'react-native';
import { widthPercentageToDP as wp,heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class HeadSukses extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false,
        }
    }
    render(props){
        return(
        <View>
            <View style={{ height: 270, width: '100%', backgroundColor: '#2d9cdb', alignItems: 'center' }}>
                <Text style={{ fontSize: 16, color: '#ffffff', marginTop: 26 }}>Transaksi Berhasil</Text>
                <View style={{ marginTop: 23, alignItems: 'center' }}>
                    <Image source={require('../../assets/images/successicon.png')} style={{ width: 80, height: 80 }} />
                    <Text style={{ marginTop: 20, color: '#ffffff' }}>24 feb 2020 18:11</Text>
                    <Text style={{ color: '#ffffff' }}>ID. Transaksi FT982383131</Text>
                    <View style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'space-between', }}>
                        <View style={{ height: 24, width: 140, borderRadius: 24, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', borderWidth: 1, borderColor: '#ffffff', marginRight: 14, marginBottom: 22 }}>
                            <Image source={require('../../assets/icons/download.png')} style={{ width: 12, height: 12 }} />
                            <Text style={{ fontSize: 12, color: '#ffffff', marginLeft: 5 }}>Simpan</Text>
                        </View>
                        <View style={{ height: 24, width: 140, borderRadius: 24, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', borderWidth: 1, borderColor: '#ffffff' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ show: true })}
                            >
                                <View style={{ backgroundColor: '', flexDirection: 'row' }}>
                                    <Image source={require('../../assets/icons/share.png')} style={{ width: 12, height: 12 }} />
                                    <Text style={{ fontSize: 12, color: '#ffffff', marginLeft: 5 }}>Bagikan</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
            <Modal visible={this.state.show} animationType="slide" transparent={false}>
                <View style={styles.modalContent}>
                    <View style={{ backgroundColor: 'white', marginTop: hp('53%'), borderRadius: 10, height: hp('37%') }}>
                        <Text style={{ fontSize: 14, marginLeft: 16, marginTop: 24 }}>Bagikan bukti transaksi ke:</Text>
                        <View style={{ borderWidth: 1, width: '100%', borderColor: '#e0e0e0', marginTop: 24 }} />
                        <View style={{ flexDirection: 'row', marginLeft: 35, marginTop: 24 }}>
                            <View style={{ marginRight: 34 }}>
                                <Image source={require('../../assets/images/wa.png')} style={{ height: 40, width: 40 }} />
                                <Text style={{ marginTop: 10, color: '#4f4f4f' }}>WhatsApp</Text>
                            </View>
                            <View>
                                <Image source={require('../../assets/images/telegram.png')} style={{ height: 40, width: 40 }} />
                                <Text style={{ marginTop: 10, color: '#4f4f4f' }}>Telegram</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={{ marginTop: 18, paddingLeft: 16 }} onPress={() => this.setState({ show: !this.state.show })} >
                            <Text style={{ color: 'red', fontSize: 15, textAlign: 'justify' }}>Batal</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
        )
    }
}

const styles=StyleSheet.create({
    modalContent:{
            backgroundColor:'#000000aa',
            padding:10,
            flex:1,
    },
})

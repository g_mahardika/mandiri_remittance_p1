import React, { Component } from 'react';
import { Image, ScrollView ,StyleSheet, Text,SafeAreaView, TouchableOpacity, View, TextInput,Picker,Modal, TouchableHighlight, Button } from 'react-native';
import {BoxWordsImage} from '../organisms';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box';
import { 
    HeaderTab, TextAndInput,
    ButtonNextFooter,
} from '../atom';


export default class BayarTagihanPicker extends Component {
    constructor(props){
        super(props)
        this.state={
            pilihHubungan:'',
            isChecked: false,
            pickerSelection:'Pilih Hubungan :',
            pickerDisplayed:false,
        }
    }

    setPickerValue(newValue){
        this.setState({
            pickerSelection:newValue
        });
        this.togglePicker();
    }

    togglePicker(){
        this.setState({
            pickerDisplayed:!this.state.pickerDisplayed
        })
    }
   
    render(){
        const pickerValues= [
            {title:'Ayah', value:'Ayah',},
            {title:'Ibu', value:'Ibu',},
            {title:'Saudara Laki-laki', value:'Saudara Laki-laki',},
            {title:'Saudara Perempuan', value:'Saudara Perempuan',}
        ]
        return(
            <View>
                
            </View>
        )
    }
}

const styles= StyleSheet.create({
    headerTitleBox:{
        width:'100%',
        flexDirection:'row',
        paddingLeft:18,
        paddingVertical:18
    },
    arrow:{
        width:21,
        height:20,
        top:2,
        right:8,
    },
    headerTitle:{
        fontSize:16,
        lineHeight:20,
        width:268,
    },
    txt:{
        marginTop:15,
        fontSize:16,
        color:'#153E74',
        fontWeight:'bold',
        lineHeight:20,
        textAlign:'center'
    },
    txt2:{
        fontSize:12,
        lineHeight:15,
    },
    touch:{
        width:'100%',
        borderWidth:1,
        borderColor:'#BDBDBD',
        borderRadius:4,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        marginTop:14,
        paddingVertical:12,
        backgroundColor:"#153e74"
    },
    box:{
        marginTop: 8,
        borderWidth: 1,
        borderColor: "black",
        borderRadius: 2,
        width: '100%',
        height: 46,
        paddingHorizontal: 5,
        paddingVertical: 14, 
        // backgroundColor:'green',
    },
    boxAlamat:{
        marginTop:8,
        borderWidth:1,
        borderColor:"black",
        borderRadius:2,
        width:'100%',
        height:80,
        paddingLeft:14,
        paddingVertical:12, 
    },
    miniBox:{
        borderWidth:2,
        borderColor:'#6e6b65',
        height:18,
        width:18,
        marginRight:11,
    }
})
import React, {Component} from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, TextInput } from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';


class MainHomeHead extends Component{
    render(props){
        return(
            <View style={styles.head}>
                <Image source={require('../../assets/images/MandiriBlue.png')} style={styles.img}  />
                <Image source={require('../../assets/icons/notif.png')} style={styles.icn} />
            </View>
        )
    }
}
export default MainHomeHead;

const styles=StyleSheet.create({
    head:{
        flexDirection:'row',
        height:65,
        // backgroundColor:'#bdbdbd',
        paddingRight:20,
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        width:'100%',
    },
    icn:{
        width:17,
        height:20,

    },
    img:{
        marginRight:100,
        width:80,
        height:50,
        marginLeft:wp('38%'),
        resizeMode:'contain',
    }
})
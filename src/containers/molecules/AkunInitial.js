import React, { Component } from 'react';
import {Image, View,Text, TouchableOpacity} from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
 
export default class AkunInitial extends Component{
    render(){
        return(
            <View>
                <View style={{ flexDirection: 'row', marginTop: 20, height: 40, backgroundColor: '' }}>
                    <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 16 }}>
                        {/* <Image source={require('../../assets/icons/katasandiicon.png')} style={{ resizeMode: 'contain', width: 26, marginRight: 18, height: 20, }} /> */}
                        <Image source={this.props.img} style={{ resizeMode: 'contain', width: 26, marginRight: 18, height: 20, }} />
                        <Text style={{ marginTop: 4, fontSize: 13 }}>{this.props.txt1}</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate(`${this.props.txt2}`)} style={{ flexDirection: 'row' }}>
                        {/* <Text style={{marginTop:2}}>{this.props.txt2}</Text> */}
                        <Image style={{ resizeMode: 'contain', width: 26, marginHorizontal: wp( '2%'), height: 17, }} source={this.props.img2} />
                    </TouchableOpacity>
                </View>
                {/* <View style={{ marginTop: 5, marginLeft:wp('5%'), width: wp('80%'), height: 2, backgroundColor: '#e0e0e0' }} /> */}
            </View>
        )
    }
}
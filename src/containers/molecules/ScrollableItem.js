import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView, ImageBackground} from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export default class ScrollableItem extends Component{
    render(){
        return(
            <View style={styles.screen}>
                <Text style={styles.txt1}>{this.props.title}</Text>
                <Text style={styles.txt2}>{this.props.txt}</Text>
                <Image source={this.props.img} style={styles.img} />
                <Text style={{ marginTop: 8, fontSize: 12 }}> {'<< '} geser ke kiri untuk pelajari lebih lanjut</Text>
            </View>
        )
    }
}

const styles= StyleSheet.create({
    txt2:{
        textAlign:'center',
        width:312,
        height:50,
    },
    img:{
        width: wp ('90%'),
        height: hp ('50%'),
        resizeMode:'contain',
        // backgroundColor:'yellow'
    },
    screen:{
        alignItems:'center',
        paddingHorizontal:16,
        // backgroundColor:'green',
        // width:'90%'
    },
    txt1:{
        fontSize:20,
        color:'#153e74',
        marginBottom:14,
    }
})
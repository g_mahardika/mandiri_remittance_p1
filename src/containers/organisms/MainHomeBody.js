import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native';
import RemittanceMenu from './RemittanceMenu';
import LastFiveTransaction from './LastFiveTransaction';

class MainHomeBody extends Component{
    constructor(props){
        super(props);
    }

    render(props){
        return (
            <ScrollView style={styles.bdy}>
                <View style={{ paddingHorizontal: 16 }}>
                    <Image source={require('../../assets/images/bg.png')} style={styles.img} />
                    <View style={{ position: 'absolute' }}>
                        <Text style={styles.txt}>Saldo Tersedia</Text>
                        <View style={{ flexDirection: 'row', marginTop: 8 }}>
                            <Text style={styles.txt2}>RM. </Text>
                            <Text style={{ fontSize: 20, color: 'white', fontWeight: 'bold' }}> 293,67</Text>
                            <Image source={require('../../assets/icons/downicon.png')} style={{ width: 22, marginLeft: 20, height: 22 }} />
                        </View>
                        <Text style={{ fontSize: 12, color: 'white', marginTop: 29, marginLeft: 40 }}>1243 5512 8731</Text>
                    </View>
                    <Text style={{ marginTop: 24, fontSize: 14, fontWeight: 'bold' }}>Remittance Menu</Text>
                </View>

                <RemittanceMenu navigation={this.props.navigation}/>

                <View style={{ width: '100%', borderWidth: 1, borderColor: '#e0e0e0', marginTop: 24 }} />
                <View style={{ paddingLeft: 16, paddingRight: 24 }}>
                    <Text style={{ marginVertical: 24 }}>5 Transaksi Terakhir</Text>
                    <LastFiveTransaction navigation={this.props.navigation} />
                </View>
            </ScrollView>
        )
    }
}
export default MainHomeBody;

const styles=StyleSheet.create({
    bdy:{
        flex:1,
    },
    img:{
        width:"100%",
        height:128,
        borderRadius:8,
        marginTop:18,
    }, 
    txt:{
        fontSize:10, 
        color:'#ffffff', 
        marginTop:38,
        marginLeft:40,
    },
    txt2:{
        fontSize:20, 
        color:'#ffffff', 
        marginLeft:40,
    }
})
import React, { Component } from 'react';
import { Image, StyleSheet,Text, TextInput, TouchableOpacity, View, Modal } from 'react-native';

export default class PembayaranTagihanKonfirmasi extends Component{
    render(props){
        return(
            <View >
                <View >
                    
                    <View style={{borderWidth:0,borderRadius:3,height:40,width:'100%',backgroundColor:'#e0e0e0', paddingHorizontal:16,paddingTop:12,}}>
                        <Text style={{fontSize:12,fontWeight:'bold',color:'#153e74'}}>{this.props.txt1}</Text>
                    </View>
                     <View style={{marginTop:24,paddingLeft:16}}>
                        <Text style={{color:'#828282',fontSize:10}}>{this.props.txt2}</Text>
                        <Text style={{fontSize:12}}>{this.props.txt3}</Text>
                        <Text style={{marginTop:20,color:"#828282" , fontSize:10,}}>{this.props.txt4}</Text>
                        <Text style={{marginTop:8,fontSize:12}}>{this.props.txt5}</Text>
                        <Text style={{fontSize:12, fontWeight:'bold'}}>{this.props.txt6}</Text>
                        <Text style={{marginTop:20, color:'#828282'}}>{this.props.txt7}</Text>
                        <Text style={{marginTop:8, fontSize:12}}>{this.props.txt8}</Text>
                    </View>
                   <View style={{marginTop:24,height:1, backgroundColor:'#e0e0e0'}}/>
                    <View style={{marginTop:24, marginHorizontal:16}}>
                        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                            <Text style={{color:'#828282', fontSize:12,}}>{this.props.txt9}</Text>
                            <Text style={{fontSize:12}}>{this.props.text1}</Text>
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:21}}>
                            <Text style={{color:'#828282', fontSize:12,}}>{this.props.text2}</Text>
                            <Text style={{fontSize:12}}>{this.props.text3}</Text>
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'space-between',marginTop:21}}>
                            <Text style={{color:'#828282', fontSize:12,}}>{this.props.text4}</Text>
                            <Text style={{fontSize:12}}>{this.props.text5}</Text>
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'space-between',marginTop:21}}>
                            <Text style={{fontWeight:'bold', fontSize:12,}}>{this.props.text6}</Text>
                            <Text style={{fontWeight:'bold',fontSize:12}}>{this.props.text7}</Text>
                        </View>
                        
                    </View>
                </View>
            </View>
        )
    }
}
import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native';
import { FiveTransaction } from '../molecules';

class LastFiveTransaction extends Component{
    render(props){
        return (
            <View style={{ marginBottom: 50 }}>
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Top Up" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="+ RM." txt5="30,21"
                    img={require('../../assets/icons/in.png')} style={{color:'#27ae60'}}

                />
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Kirim Uang - Orang Tua" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="- RM." txt5="30,21"
                    img={require('../../assets/icons/out.png')} style={{color:'#EB5757'}}

                />
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Top Up" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="+ RM." txt5="30,21"
                    img={require('../../assets/icons/in.png')} style={{color:'#27ae60'}}
                />
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Kirim Uang - Orang Tua" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="- RM." txt5="30,21"
                    img={require('../../assets/icons/out.png')} style={{color:'#EB5757'}}
                />
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Top Up" txt2="FT200145L791" txt3="Rabu, 06 Februari '20, 18:45" txt4="+ RM." txt5="30,21"
                    img={require('../../assets/icons/in.png')} style={{color:'#27ae60'}}
                />
            </View >
        )
    }
}
export default LastFiveTransaction;

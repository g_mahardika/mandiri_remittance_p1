import React, {Component} from 'react';
import { View,Image,StyleSheet,TextInput,Text,TouchableOpacity,TouchableHighlight,Picker,Modal} from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export default class PembayaranTagihanAll extends Component{
    constructor(props){
        super(props)
        this.state={
            show:false,
            pdam:'',
            pilihHubungan:'',
            isChecked: false,
            pickerSelection:`${this.props.txt4} :`,
            pickerDisplayed:false,
        }
    }

    setPickerValue(newValue){
        this.setState({
            pickerSelection:newValue
        });
        this.togglePicker();
    }

    togglePicker(){
        this.setState({
            pickerDisplayed:!this.state.pickerDisplayed
        })
    }
    render(props){
        const pickerValues= [
            {title:`${this.props.txt6}`, value:`${this.props.txt6}`},
            {title:`${this.props.txt7}`, value:`${this.props.txt7}`,},
            {title:`${this.props.txt8}`, value:`${this.props.txt8}`,},
            {title:`${this.props.txt9}`, value:`${this.props.txt9}`,}
        ]
        return (
            <View>
                <View style={{ marginHorizontal: 16, height: 106, marginTop: 24 }}>
                    <Image source={this.props.img} style={{ resizeMode: 'contain', width: '100%', height: 106 }} />
                </View>
                <View style={{ borderColor: '#e0e0e0', marginTop: 14, borderWidth: 1, width: '100%' }} />
               <View style={{ marginTop: 24, marginHorizontal: 16 }}>
                    <Text style={{ fontSize: 12, marginBottom: 8 }}>{this.props.txt1}</Text>
                    <View style={{ flexDirection: 'row', width: '100%', height: 40, borderWidth: 1, borderColor: '#828282', paddingHorizontal: 16 }}>
                        <TextInput placeholder={`${this.props.txt2}`} style={{ flex: 1 }} />
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(`${this.props.navigasi}`)}>
                            <Image source={this.props.img2} style={{ width: 24, resizeMode: 'contain' }} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ marginTop: 24, marginHorizontal: 16 }}>
                    <Text style={{ fontSize: 12, marginBottom: 8 }}>{this.props.txt3}</Text>
                    <View style={{ flexDirection: 'row', width: '100%', height: 40, borderWidth: 1, borderRadius: 3, borderColor: '#828282', paddingHorizontal: 16, justifyContent:'space-between' }}>
                        <Text style={{ fontSize: 14, marginTop:10 }}>{this.state.pickerSelection}</Text>
                        <TouchableOpacity onPress={()=> this.togglePicker()}>
                            <Image source={require('../../assets/icons/down.png')} style={{ resizeMode: 'contain', width: 20, height: 20, marginTop: 10 }} />
                        </TouchableOpacity>
                    </View>
                    <Modal visible={this.state.pickerDisplayed} transparent={true} animationType={'slide'} >
                            <View style={{ backgroundColor: '#000000aa', flex: 1, padding: 10,paddingBottom: hp('10%'),}}>
                                <View style={{ backgroundColor: 'white',height:0, alignSelf: 'center', flex: 1, marginTop: hp('70%'), width: wp ('75%'), borderRadius: 10, }}>
        <Text style={{ marginBottom: 10, fontSize: 15, fontWeight: 'bold', textAlign: 'center',alignItems:'center' }}>{`${this.props.txt10}`}</Text>
                                    { pickerValues.map((value, index) => {
                                        return (
                                            <TouchableHighlight onPress={() => this.setPickerValue(value.value,key=index)} on style={{ paddingHorizontal: 4, alignItems: 'center' }}>
                                                <Text style={{ fontSize: 15 }}>{value.title}</Text>
                                            </TouchableHighlight>
                                        )
                                    })}
                                </View>
                            </View>
                        </Modal>
                    
                </View>
            </View>
        )
    }
}


const styles=StyleSheet.create({
    modalContent:{
            backgroundColor:'#000000aa',
            padding:10,
            flex:1,
    },
})


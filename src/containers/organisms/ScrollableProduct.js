import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView, ScrollView} from 'react-native';
import {ScrollableItem} from '../molecules';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';


export default class ScrollableProduct extends Component {
    render(){
        return(
            <ScrollView horizontal style={styles.body}>
                <ScrollableItem
                    title="Remittance Platforms"
                    txt="Sistem pengiriman uang bagi pekerja imigran di Luar Negeri ke Indonesia" 
                    img={require('../../assets/images/illustrasi1.png')}
                />
                <ScrollableItem
                    title="Kirim Uang Lebih Mudah" 
                    txt="Sekarang proses pengiriman uang lebih mudah, karena dapat Anda lakukan kapanpun dan dimana pun." 
                    img={require('../../assets/images/illustrasi2.png')}
                />
                <ScrollableItem
                    title="Pembayaran Tagihan" 
                    txt="Selain dapat melakukan pengiriman uang, Anda juga dapat melakukan pembayaran tagihan di Indonesia." 
                    img={require('../../assets/images/illustrasi3.png')}
                />
            </ScrollView>
        )
    }
}

const styles=StyleSheet.create({
    body:{
        flexDirection:'row',
        width:wp ('97%'), 
        marginRight: 5,
    }
})
import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, TextInput, ScrollView } from 'react-native';
import {RemittanceMenuBasic,} from '../molecules'

class RemittanceMenu extends Component{
    constructor(props){
        super(props);

    }
    render(){
        return (
            <View>
                <View style={styles.box}>
                    <RemittanceMenuBasic 
                        navigation={this.props.navigation} 
                        txt2="KirimUang"
                        style={{ backgroundColor: '#ff6951', borderColor: 'transparent' }} 
                        img={require('../../assets/icons/kirimuangicon.png')} 
                        txt="Kirim Uang" />
                    <RemittanceMenuBasic 
                        navigation={this.props.navigation} 
                        txt2="PembayaranTagihan" 
                        style={{ backgroundColor: '#1ccd9d', borderColor: 'transparent' }} 
                        img={require('../../assets/icons/tagihanicon.png')} 
                        txt="Pembayaran Tagihan" />
                </View>
                <View style={styles.box2}>
                    <RemittanceMenuBasic 
                        navigation={this.props.navigation} 
                        txt2="Kalkulator"
                        style={{ backgroundColor: '#fcaf2a', borderColor: 'transparent' }} 
                        img={require('../../assets/icons/matauangicon.png')} 
                        txt="Kalkulator Ringgit-Rupiah" />
                    <RemittanceMenuBasic 
                        navigation={this.props.navigation} 
                        txt2="Lain"
                        style={{ backgroundColor: '#466ffe', borderColor: 'transparent' }} 
                        img={require('../../assets/icons/lainicon.png')} 
                        txt="Lain-lain" />
                </View>
            </View>
        )
    }
}
export default RemittanceMenu;

const styles = StyleSheet.create({
    box:{
        flexDirection:'row',
        marginTop:14,
        marginBottom:16,
        flex:1,
    },
    box2:{
        flexDirection:'row',
        marginTop:0,
    }
})
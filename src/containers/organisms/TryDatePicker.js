import React, {Component} from "react";
import { Button, View, Text, TouchableOpacity,StyleSheet } from "react-native";
import moment from 'moment';
import DateTimePicker from "react-native-modal-datetime-picker";

export default class TryDatePicker extends Component{
  constructor(props){
    super(props)
    this.state = {
      isVisible:false,
      chosenDate:'',
    }
    this.handlePicker = this.handlePicker.bind(this)
    this.showPicker = this.showPicker.bind(this)
    this.hidePicker = this.hidePicker.bind(this)
  }
  
  handlePicker=(date)=>{
    this.setState({
      isVisible:false,
      chosenDate:moment(date).format('Do MMM YYYY'),
    })
  }
  
  showPicker=(props)=>{
    this.setState({
      isVisible:true,
    })
  }
  
  hidePicker=()=>{
    this.setState({
      isVisible:false,
    })
  }
  
  render(props){
    
    return (
      <View style={styles.container}>
        <Text style={{color:'blue', fontSize:12}}>
          {this.state.chosenDate}
        </Text>
        <TouchableOpacity style={styles.button} onPress={this.showPicker}>
          <Text style={styles.text}>Show DatePicker</Text>
        </TouchableOpacity>

          <DateTimePicker
            isVisible={this.state.isVisible}
            onConfirm={this.handlePicker}
            onCancel={this.hidePicker}
            mode={'date'}
            datePickerContainerStyleIOS = {'spinner'}
            datePickerModeAndroid={'spinner'}
      
          />
      </View>
    );
  }
};



const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#f5fcff',
  },
  button:{
    width:250,
    height:50,
    backgroundColor:'#330066',
    borderRadius:30,
    justifyContent:'center',
    marginTop:15,
  },
  text:{
    fontSize:18,
    color:'white',
    textAlign:'center',
  }
})
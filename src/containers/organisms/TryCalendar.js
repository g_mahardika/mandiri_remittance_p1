import React,{Component} from 'react';
import DatePicker from 'react-native-datepicker';

export default class TryCalendar extends Component {
  constructor(props){
    super(props)
    this.state={
      date:"30-03-2020"
    }
  }

  render(props){
    return(
      <DatePicker
        navigation={this.props.navigation}
        style={{width:200}}
        date={this.state.date}
        mode='date'
        placeholder='Pilih Tanggal'
        format='DD-MM-YYYY'
        minDate='01-01-2000'
        maxDate='01-01-2025'
        confirmBtnText='Pilih'
        cancelBtnText='Batal'
        customStyles={{
          dateIcon:{
            position:'absolute',
            left:0,
            top:4,
            marginLeft:0
          },
          dateInput:{
            marginLeft:36
          }
          
        }}
        onDateChange={(date)=>{this.setState({date:date})}}
      />
    )
  }
}
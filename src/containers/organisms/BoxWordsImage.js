import React, { Component } from 'react';
import {TouchableOpacity, StyleSheet,Image,Text, TextInput,View} from 'react-native';
import {BoxImage, InlineWords} from '../atom';

export default class BoxWordsImage extends Component{
    render(props){
        return (
            <View>
                <View style={{ marginTop: '1.0%', }}>
                    <InlineWords txt1="Foto Selfie" txt2="Lihat Contoh" />
                    <BoxImage navigation={this.props.navigation} />
                </View>
                <View style={{ marginTop: 0, }}>
                    <InlineWords txt1="Foto Passport(Halaman Depan)" txt2="Lihat Contoh" />
                    <BoxImage navigation={this.props.navigation}/>
                    <View style={styles.greyBox} ><Text style={styles.txt}>No Passport (Otomatis)</Text></View>
                </View>
                <View style={{ marginTop: 0, }}>
                    <InlineWords txt1="Foto Passport(Halaman Visa)" txt2="Lihat Contoh" />
                    <BoxImage navigation={this.props.navigation}/>
                    <View style={styles.greyBox} ><Text style={styles.txt}>No Visa (Otomatis)</Text></View>
                </View>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    greyBox:{
        width:'100%',
        borderColor:"#828282",
        backgroundColor:'#e0e0e0',
        borderWidth:1,
        height:40,
        paddingVertical:12,
        paddingLeft:14,
    },
    txt:{
        fontSize:12,
        color:'grey'
    }
})
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import {
    MainHome,RiwayatTransaksi,
    AkunUbahSandi,AkunUbahPIN,
    AkunPINBaru,
    AkunKonfirmasiPIN,
    TentangAplikasi,
    HubungiKami,
} from '../../components'

import {
    KirimUang,
    KirimUang2,
    KirimUangKonfirmasi,
    KirimUangPIN,
    KirimUangSukses,
    Kalkulator,
    Lain,
    LainComingSoon,
} from '../../components/Main/RemittanceMenu/index'

import {
    TryCalendar, TryDatePicker,
    FilterTransaksi,TryBarcode,
} from '../../containers/organisms'

import {
    BottomKonfirmasi,
} from '../../containers/molecules'

import {
    PembayaranTagihan,
    TagihanPLN,
    TagihanPLNKonfirmasi,
    TagihanPLNPIN,
    TagihanPLNSukses,
    TagihanPDAM,
    TagihanPDAMKonfirmasi,
    TagihanPDAMPIN,
    TagihanPDAMSukses,
    TagihanBPJS,
    TagihanBPJSKonfirmasi,
    TagihanBPJSPIN,
    TagihanBPJSSukses,
} from '../../components/Main/RemittanceMenu/PembayaranTagihan'

const MainStackNavigation = createStackNavigator(
    {
        MainHome,
        TryCalendar,
        TryDatePicker,
        RiwayatTransaksi,
        FilterTransaksi,
        KirimUang,
        KirimUang2,
        KirimUangKonfirmasi,
        KirimUangPIN,
        KirimUangSukses,
        PembayaranTagihan,
        TagihanPLN,
        TryBarcode,
        TagihanPLNKonfirmasi,
        BottomKonfirmasi,
        TagihanPLNPIN,
        TagihanPLNSukses,
        TagihanPDAM,
        TagihanPDAMKonfirmasi,
        TagihanPDAMPIN,
        TagihanPDAMSukses,
        TagihanBPJS,
        TagihanBPJSKonfirmasi,
        TagihanBPJSPIN,
        TagihanBPJSSukses,
        Kalkulator,
        Lain,
        LainComingSoon,
        AkunUbahSandi,
        AkunUbahPIN,
        AkunPINBaru,
        AkunKonfirmasiPIN,
        TentangAplikasi,
        HubungiKami,
    },
    {
        headerMode:'none',
        initialRouteParams:'MainHome',
    }
)

const MainStackNavigator = createAppContainer(MainStackNavigation);
export default MainStackNavigator;
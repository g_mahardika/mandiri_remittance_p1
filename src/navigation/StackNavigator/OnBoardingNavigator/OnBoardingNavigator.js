import {createSwitchNavigator, createAppContainer} from 'react-navigation';
// import {createStackNavigator} from 'react-navigation-stack';
import {Opening, Greeting} from '../../../components';
import SendRegistrationStack from '../SendRegistrationStack';
import MainStackNavigator from '../MainStackNavigation';
import MainTabNavigator from '../../TabNavigator/MainTabNavigator';

const onBoardingNavigator = createSwitchNavigator({
    Opening,
    Greeting,
    SendRegistrationStack,
    MainTabNavigator,
}, {
    initialRouteName: 'Opening',
    headerMode: 'none'
})

const BoardingNavigator = createAppContainer(onBoardingNavigator);

export default BoardingNavigator;


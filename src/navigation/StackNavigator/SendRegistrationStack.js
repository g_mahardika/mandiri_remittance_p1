import { createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {
    AccountAndRegistration,
    AccountAndRegistration2,
    AccountAndRegistration3,
    AccountAndRegistration4,
    AccountAndRegistration5,
    AccountAndRegistration6,
    LoginForm,
} from '../../components';
import {
    TryCamera2,
    TryFingerprint,
} from '../../containers/organisms'



const RegistrationStack = createStackNavigator({
    AccountAndRegistration,
    TryCamera2,
    AccountAndRegistration2,
    AccountAndRegistration3,
    AccountAndRegistration4,
    AccountAndRegistration5,
    AccountAndRegistration6,
    LoginForm,
    TryFingerprint,
},
{
    headerMode:'none',
    initialRouteParams:'AccountAndRegistration',
})

const SendRegistrationStack = createAppContainer(RegistrationStack);

export default SendRegistrationStack;
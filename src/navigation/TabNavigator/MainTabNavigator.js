// tabBarIcon:({focused})=>(
//     focused 
//     ? <Image source={require('../../assets/icons/homeiconaktif.png')} style={styles.NavIcon} />
//     : <Image source={require('../../assets/icons/homeicon.png')} style={styles.NavIcon} />
// )
import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import {MainHome, RiwayatTransaksi, Akun, } from '../../components';
import MainStackNavigator from '../StackNavigator/MainStackNavigation'

const MainTabNavigator = createBottomTabNavigator({
  Home:{
    screen: MainStackNavigator,
    navigationOptions:{
      tabBarLabel:'Home',
      tabBarIcon:({focused})=>(
          focused
          ?
        <Image
          source={require('../../assets/icons/homeiconaktif.png')}
          resizeMode='contain'
          style={styles.icons}
        />
        :
        <Image
            source={require('../../assets/icons/homeicon.png')}
            resizeMode='contain'
            style={styles.icons}
        />
      )
    }
  } ,
  Transaksi:{
    screen:RiwayatTransaksi,
    navigationOptions:{
      tabBarLabel:'Transaksi',
      tabBarIcon:({focused})=>(
          focused
              ?
              <Image
                  source={require('../../assets/icons/transaksiiconaktif.png')}
                  resizeMode='contain'
                  style={styles.icons}
              />
              :
              <Image
                  source={require('../../assets/icons/transaksiicon.png')}
                  resizeMode='contain'
                  style={styles.icons}
              />
      )
    }
  } ,
  Notify: {
    screen: Akun,
    navigationOptions:{
      tabBarLabel:'Akun',
      tabBarIcon:({focused})=>(
        focused
              ?
              <Image
                  source={require('../../assets/icons/akuniconaktif.png')}
                  resizeMode='contain'
                  style={styles.icons}
              />
              :
              <Image
                  source={require('../../assets/icons/akunicon.png')}
                  resizeMode='contain'
                  style={styles.icons}
              />
        )
    }
  }
},{
  tabBarOptions:{
    labelStyle:{
      fontSize:15,
    }
  }
});

export default createAppContainer(MainTabNavigator);

const styles=StyleSheet.create({
  icons:{
    width: 20, 
    height: 20,
    marginTop:10,
  }
})

import React from 'react';

import {Provider} from 'react-redux';
import store from './try/MANDIRI_REMITTANCE/store/store';
import MandiriNavigation from './try/MANDIRI_REMITTANCE/navigasi/MandiriNavigation';

const App = () => {
  return (
    <Provider store={store}>
      <MandiriNavigation/>
    </Provider>
  );
};

export default App;

